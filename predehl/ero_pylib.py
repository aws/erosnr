# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 15:47:49 2020
@author: Peter
"""
from astropy.io import fits
from astropy.coordinates import SkyCoord as co
from astropy import units as u 
from astropy.table import Table
from stingray import Lightcurve, Powerspectrum
from stingray.events import EventList
from stingray.pulse.search import z_n_search
from stingray.pulse.pulsar import fold_events
from stingray.pulse.search import plot_profile
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
#-------------------------------------------------------------------------------#
#                                                                               # 
#   READS EVENTS FROM FITSFILE                                                  #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_read_fits(infile):                                                      # read events table from fits-file
    with fits.open(infile, memmap=False) as hdu:                                # with open fits file
        data = hdu[1].data                                                      # open extension 1 (=data)   
    return(data)                                                                #  return data table  
#-------------------------------------------------------------------------------#
#                                                                               # 
#   WRITE EVENTS TO FITSFILE                                                    #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_write_fits(data, outfile):                                              # write events to fits file
    t = Table(data)                                                             # make astropy table out of data
    t.write(outfile, overwrite=True)                                            # write fits file
    #t.close()
    return()           
#-------------------------------------------------------------------------------#
#                                                                               # 
#   REMOVES SUPERFLUOUS COLUMNS FROM DATA TABLE                                 #
#                                                                               #
#-------------------------------------------------------------------------------#
def rem_table(data):                                                            # the following is clear
    t = Table(data)
    t.remove_columns('FLAG')
    t.remove_columns('FRAMETIME')
    t.remove_columns('RECORDTIME')
    t.remove_columns('EV_WEIGHT')
    t.remove_columns('SUBX')
    t.remove_columns('SUBY')
    t.remove_columns('PHA')
    t.remove_columns('PAT_TYP')
    t.remove_columns('PAT_INF')
    return(t)
#-------------------------------------------------------------------------------#
#                                                                               # 
#   SELECT OPTIONALLY ENERGY RANGE, CIRCLE REGION, ANNULUS REGION,              #
#          TELESCOPE MODULES, TIME INTERVAL, DETECTOR EXTRACT RADIUS,           #
#          CUTS OUT REGIONS INTERACTIVELY, EXCLUDE TIME INTERVAL                #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_select(data, ERGRANGE=None, TM=None, EXTIME=None, CUT=None,             # select events from input file according to  
              CIRCLE=None, ANNULUS=None, TI=None, DETRING=None,
              BRIGHTPIXEL=None, PATTERN=None):                                                # keyword parameters
    index = np.ones(len(data['RA']))                                            # create an index field all with ones
    print('STARTVALUE: ', len(index), 'counts')                             
    if (ERGRANGE):                                                              # if energy range is given
        index = index * (data['PI'] < ERGRANGE[1])                              # zero elements above upper energy
        index = index * (data['PI'] > ERGRANGE[0])                              # zero elements below lower energy
        print('ENERGY: ', len(index.nonzero()[0]), 'counts')
    if (TI):                                                                    # if time interval range is given
        index = index * (data['TIME'] < TI[1])                                  # zero elements above interval
        index = index * (data['TIME'] > TI[0])                                  # zero elements below interval
        print('TIME INTERVAL',  len(index.nonzero()[0]), 'counts')
    if (PATTERN):
        j = np.setdiff1d([1,2,3,4],PATTERN)
        for i in j:
            index = index * (data['PAT_TYP'] !=i)
    if (CIRCLE):                                                                # if circle region is given
        corr = np.cos(CIRCLE[1]*3.14/180.)  
        dist = np.sqrt( np.square((data['RA']-CIRCLE[0])*corr) +                # calculate distance from centroid 
                   np.square(data['DEC']-CIRCLE[1]) )  
        index = index * (dist<CIRCLE[2])                                        # zero elements outside of circle
        if len(CIRCLE) == 4: index = index * (dist>CIRCLE[3])                   # zero elements inside if given
        print('CIRCLE: ',  len(index.nonzero()[0]), 'counts')   
    if (DETRING):                                                               # if annulus region is given
        dist = np.sqrt(np.square(data['RAWX']-192.) +                           # calculate distance from detectror
               np.square(data['RAWY']-192.))                                    #     centre (192,192)
        index = index * (dist>DETRING[0])                                       # zero elements below ring
        index = index * (dist<DETRING[1])                                       # zero elements beyond ring
        print('DETRING: ',  len(index.nonzero()[0]), 'counts')
    if (TM):                                                                    # if telescope modules are given
        j = np.setdiff1d([1,2,3,4,5,6,7], TM)                                   # find TMs to be discarded (clear how?)
        for i in j:                                                             # for all TMs to be discarded
            index = index*(data['TM_NR']!=i)                                    # zero indices of those TMs
        print('TELECOPE MODULE: ',  len(index.nonzero()[0]), 'counts')
    if (CUT):         
        x = data['X'];      y = data['Y']                                       # works only with sky coords X,Y!!!
        x0 = CUT[0];     y0 = CUT[1];     r = CUT[2]*3600*20                    # save circle temporarily
        d = np.sqrt( (x-x0)**2 + (y-y0)**2 )                                    # distance from central point
        index = index * (d > r)                                                 # false (=0) within circle                    
        print('cut out: ', x0, y0, r, len(data))
    data = data[index.nonzero()]   
                                             # reduce data to selected indices
#-------------------------------------------------------------------------------# reverse logic, therefore separate:
    if (BRIGHTPIXEL):                                                           # exclude bright pixel area
        index = np.ones(len(data['PI']))                                        # create again an index field all with ones
        index = index * (data['RAWX'] > BRIGHTPIXEL[0])                         # false left of area
        index = index * (data['RAWX'] < BRIGHTPIXEL[1])                         # false right of area
        index = index * (data['RAWY'] > BRIGHTPIXEL[2])                         # false below of area
        index = index * (data['RAWY'] < BRIGHTPIXEL[3])                         # false above the area
        index = np.int64(np.absolute(index-1))                                  # reverse logic!
        data = data[index.nonzero()]                                            # discard all elements within area
        print('bright pixel: ', len(data))
    if (EXTIME):                                                                # exclude time intervals after everything else: reverse logic
        index = np.ones(len(data['PI']))                                        # create again an index field all with ones
        index = index * (data['TIME'] > EXTIME[0])                              # false (=0) before interval                    
        index = index * (data['TIME'] < EXTIME[1])                              # false (=0) after after interval
        index = np.int64(np.absolute(index-1))                                  # reverse logic!
        data = data[index.nonzero()]                                            # discard all elements within interval
        print('non GTI: ', len(data))
    return(data)
#-------------------------------------------------------------------------------#
#                                                                               # 
#   CUTS AREAS (CIRCLES) INTERACTIVELY                                          #
#   REQUIRES NON-INLINE WINDOW, E.G BY %matplotlib qt5                          #
#   CAN BE TURNED BACK TO INLINE BY %matplotlib inline                          #
#                                                                               #
#-------------------------------------------------------------------------------#
def onclick(event):                                                             # callback function
    global data, outf, extr, cid, fig                                           # needed for commun. wt 'main'
    ix,iy  = event.xdata, event.ydata                                           # skyXY coordinates on click
    if not ix:                                                                  # stop process on click outside
        print(outf)
        evt_write_fits(data, outf)
        plt.disconnect(cid)                                                     # disconnect event handling
        plt.close()                                                             # close plot window
        return()
    else:                                                                       # on click inside 
        data = evt_select(data, CUT=[ix,iy,extr])                               # new selection with cut
        fig.clear()                                                             # clear figure
        plt.hist2d(data['X'], data['Y'], [500,500],                             # plot data every time on click&cut
                   cmap ='inferno', norm=LogNorm())   
        plt.colorbar()                                                              # draw color bar
        fig.canvas.draw()                                                       # refresh/update window
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def evt_interactive_cut(infile, outfile, cutrad):                               # 'main' routine
    global data, outf, extr, cid, fig                                           # needed for commun. wt callback
    outf = outfile;     extr = cutrad
    data = evt_read_fits(infile)                                                # read data from fits file
    fig = plt.figure(figsize=(12,10))                                           # create plot window
    plt.hist2d(data['X'], data['Y'], [500,500],                                 # plot data
               cmap ='inferno', norm=LogNorm()) 
    plt.colorbar()                                                              # draw color bar
    
    cid = plt.connect('button_press_event', onclick)
#                      lambda event: onclick(event, data, extrad, fig, cid, outfile))                        # start callback routine
    plt.show()
    return()
#-------------------------------------------------------------------------------#
#                                                                               #
#   CONVERT RA, DEC INTO SKY X,Y                                                #
#   WORKS BADLY IF CENTRAL REGION IS CUT, E.G. FOR PILEUP REASON                #
#                                                                               #
#-------------------------------------------------------------------------------#
def radec_to_xy(data, centre):
    dist = np.sqrt( (data['RA']-centre[0])**2 + (data['DEC']-centre[1])**2 )    # search photon event nearest to position
    index = np.argmin(dist)                                                     # this is the photon
    return(data['X'][index], data['Y'][index])
#-------------------------------------------------------------------------------#
#                                                                               #
#   CONVERT RA, DEC INTO ECLIPTIC LON, LAT                                      #
#                                                                               #
#-------------------------------------------------------------------------------#
def radec_to_lola(RA, DEC):
    c = co(ra=RA*u.degree, dec=DEC*u.degree, frame='icrs')                      # into SkyCoord frame
    d = c.geocentricmeanecliptic                                                # transform to ecliptic
    lon=d.lon;    lat=d.lat;    lon=lon.deg;    lat=lat.deg;                    # convert all and into degree
    return(lon,lat)
#-------------------------------------------------------------------------------#
#                                                                               #
#   CONVERT RA, DEC INTO GALACTIC LON, LAT                                      #
#                                                                               #
#-------------------------------------------------------------------------------#
def radec_to_galLB(RA, DEC):
    c = co(ra=RA*u.degree, dec=DEC*u.degree, frame='icrs')                      # into SkyCoord frame
    d = c.galactic                                                              # transform to galactic
    l=d.l;    b=d.b;    l=l.deg;    b=b.deg;                                    # convert all and into degree
    return(l,b)
#-------------------------------------------------------------------------------#
#                                                                               #
#   MEAN OF ALL EVENTS IN RA, DEC                                               #
#   THIS HELPS IF POSITION OF SOURCE OR FIELD IS ABSOLUTELY NOT KNOWN           #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_mean(data):
    return(np.mean(data['RA']), np.mean(data['DEC']), 5.)                       # RD_Pos, 5° around mean position
#-------------------------------------------------------------------------------#
#                                                                               #
#   AUXILIARY ROUTINE FOR CENTROIDING IN SKY X,Y COORDS                         #
#                                                                               #
#-------------------------------------------------------------------------------#
def centroid_XY(data, centre):                                                  # center and radius for centroiding, default 0,0, 20'
    X = data['X'];      Y = data['Y']                                           # save data X and Y temporarily
    x0 = centre[0];      y0 = centre[1];      rad = centre[2]  
    index=np.ones(len(X))
    print(x0,y0,len(index))
    for i in range(2):                                                          # iteratively (1=enough) finding centroid
        dist = np.sqrt( np.square(X-x0) + np.square(Y-y0) )                     # distance from previous centroid
        index = index * (dist<rad)                                              # zero elements outside of circle
        x = X[index.nonzero()]; y = Y[index.nonzero()]                          # keep only events inside circle temporarily
        x0 = np.mean(x);    y0 = np.mean(y)                                     # centroid x, y
        rad = rad/2.     
    return(x0,y0)                                                               # return centered coordinates & distance from centre
#-------------------------------------------------------------------------------#
#                                                                               #
#   AUXILIARY ROUTINE FOR CENTROIDING IN RA, DEC COORDS                         #
#                                                                               #
#-------------------------------------------------------------------------------#
def centroid_RD(data, centre):                                                  # center and radius for centroiding, default 0,0, 20'
    X = data['RA'];      Y = data['DEC']                                        # save data X and Y temporarily
    x0 = centre[0];      y0 = centre[1];      rad = centre[2]                   # save parameters temporarily
    index=np.ones(len(X))
    for i in range(1):                                                          # iteratively (1=enough) finding centroid
        dist = np.sqrt( np.square(X-x0) + np.square(Y-y0) )                     # distance from previous centroid
        index = index * (dist<rad)                                              # zero elements outside of circle
        x = X[index.nonzero()]; y = Y[index.nonzero()]                          # keep only events inside circle temporarily
        x0 = np.mean(x);    y0 = np.mean(y)                                     # centroid x, y
        rad = rad/2.                                                            # reduce search radius
    return(x0,y0)                                                               # return centered coordinates & distance from centre
#-------------------------------------------------------------------------------#
#                                                                               #
#   AUXILIARY ROUTINE FOR CENTROIDING IN DETECTOR COORDS                        #
#                                                                               #
#-------------------------------------------------------------------------------#
def centroid_detXY(data, centre):                                               # center and radius for centroiding, default 0,0, 20'
    X = data['RAWX'];      Y = data['RAWY']                                     # save data X and Y temporarily
    x0 = np.mean(X); y0 = np.mean(Y)                                            # centroid starting point
    print('DETECTOR_CENTROID: ', x0, y0)
    #index=np.ones(len(X))
    #dist = np.sqrt( np.square(X-x0) + np.square(Y-y0) )                     # distance from previous centroid
    #x = X[index.nonzero()];     y = Y[index.nonzero()]                          # keep only events inside circle temporarily
    #x0 = np.mean(x);    y0 = np.mean(y)                                     # centroid x, y
    return(x0,y0)                                                               # return centered coordinates & distance from centre
#-------------------------------------------------------------------------------#
#                                                                               #
#   CENTROIDES SKY X, Y SEPARATELY FOR ALL SELECTED TMODs                       #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_centroid_TM(data, centre):                                              # input = data and centre X, Y
    for i in range(8):                                                          # for all TMs (0-7)
        index = np.int32(np.ones(len(data['TM_NR'])))                           # create first index field with 1 
        index = index * (data['TM_NR'] == i)                                    # 1 only for current TM in loop
        index = index.nonzero()[0]                                              # reduce to valid 1s
        if len(index) > 0:
            X1, Y1 = centroid_XY(data[index], centre)                         # do centroiding
            Dx = np.int((centre[0]-X1))/20.
            Dy = np.int((centre[1]-Y1))/20.
            print('TM',i,' Deviation= ',Dx, Dy,' arcsec')
            data['X'][index] = data['X'][index] - X1                            # centralize X in data field to 0
            data['Y'][index] = data['Y'][index] - Y1                            # centralize Y in data field to zero
    return(data)                                                                # return corrected data
#-------------------------------------------------------------------------------#
#                                                                               #
#   CALCULATES DEVIATION FROM GAIA POSITIONS FOR TMs SEPARATELY                 #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_deviation_RD(data, name, centre):                                       # input = data and centre X, Y
    print('{:s}: Gaia Position RA={:8.4f} Dec ={:+8.4f}'.format(name,centre[0],centre[1]))
    for i in range(8):                                                          # for all TMs (0-7)
        index = np.int32(np.ones(len(data['TM_NR'])))                           # create first index field with 1 
        index = index * (data['TM_NR'] == i)                                    # 1 only for current TM in loop
        index = index.nonzero()[0]                                              # reduce to valid 1s
        if len(index) > 0:                                                      # data of this TM available 
            x0,y0 = centroid_RD(data[index], centre)                            # gets centroid of photons
            dx = (x0-centre[0]) * 3600. * np.cos(y0/180.*np.pi)
            dy = (y0-centre[1]) * 3600.
            print('TM {:d}: \u03B4RA = {:5.1f} arcsec   \u03B4Dec = {:5.1f} arcsec'.format(i,dx,dy))
    return()                                                                    # return corrected data
#-------------------------------------------------------------------------------#
#                                                                               # 
#   DISPLAYS IMAGE OF EVENTS FILE IN RA, DEC                                    #                   
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_ima_radec(data, NAME=None, NBINS=500, CENTRE=None):                      # 50x50 bins as default
    plt.figure(figsize=(12,10))                                                 # create plot
    print('RA Dec IMAGE:', len(data))                                           # assumes quadratic image!
    plt.gca().invert_xaxis()
    plt.hist2d(data['RA'], data['DEC'], [NBINS,NBINS],                          # image = 2dim histogram
                     cmap ='viridis', norm=LogNorm())   
    if NAME:                                                                    # if name given
        plt.suptitle(NAME, fontsize=22)                                         # plot as supertitle
    title = str(len(data))+' counts    '
    if CENTRE:                                                                  # if centre given
        x0 = CENTRE[0];   y0 = CENTRE[1];     l = CENTRE[2]/2. 
        print('Gaia Position: ',x0,y0)
        plt.plot([x0-l,x0+l],[y0,y0],'b')                                       # plot blue cross at nominal position
        plt.plot([x0,x0],[y0-l,y0+l],'b')
        x1,y1 = centroid_RD(data, CENTRE)                                       # get centroid of photons
        print('Measured Position: ',x1,y1)
        plt.plot([x1-l,x1+l],[y1,y1],'r')                                       # plot red cross at centroid
        plt.plot([x1,x1],[y1-l,y1+l],'r')
        title = title+'\n blue cross: Gaia position, red cross: centroid'
        plt.xlim(CENTRE[0]-CENTRE[2], CENTRE[0]+CENTRE[2])
        plt.ylim(CENTRE[1]-CENTRE[2], CENTRE[1]+CENTRE[2])
    plt.gca().invert_xaxis()                                                    # invert x-axis, RA from left to right
    plt.title(title, fontsize=18)                                               # plot coordinates as title
    plt.xlabel('Right Ascension [deg]', fontsize=18)                            # x-label RA
    plt.ylabel('Declination [deg]', fontsize=18)                                # y-label DEC
    plt.colorbar()                                                              # draw color bar
    plt.show()                                                                  # displays plot without waiting for others
    return()
#-------------------------------------------------------------------------------#
#                                                                               # 
#   DISPLAYS IMAGE OF EVENTS FILE IN ECLIPTIC COORDINATES                       #                   
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_ima_eclip(data, NAME=None, NBINS=500, CENTRE=None):                      # 50x50 bins as default
    plt.figure(figsize=(12,10))                                                 # create plot
    print('Eclip. IMAGE:', len(data))                                           # assumes quadratic image!
    X,Y = radec_to_lola(data['RA'], data['DEC'])                                # convert photons from RA,Dec to lon, lat
    plt.hist2d(X, Y, [NBINS,NBINS], cmap ='viridis', norm=LogNorm())            # image = 2-dim histogram
    if NAME:                                                                    # if name given
        plt.suptitle(NAME, fontsize=22)                                         # plot as suptitle
    title = str(len(data))+' counts    '
    if CENTRE:                                                                  # if CENTRE given
        x0,y0 = radec_to_lola(CENTRE[0], CENTRE[1])                             # convert from RA, Dec to lon, lat
        l = CENTRE[2]/2.                                                        # size of cross = 1/2 of image
        plt.plot([x0-l,x0+l],[y0,y0],'b')                                       # plot blue cross at nominal position
        plt.plot([x0,x0],[y0-l,y0+l],'b')
        x1, y1 = centroid_RD(data, CENTRE)                                      # get centroid of data within radius
        x2,y2 = radec_to_lola(x1,y1)                                            # convert to ecliptic coordinates
        plt.plot([x2-l,x2+l],[y2,y2],'r')                                       # plot red cross at centroid
        plt.plot([x2,x2],[y2-l,y2+l],'r')
        title = title+'\n blue cross: Gaia position, red cross: centroid'
        plt.xlim(x0-CENTRE[2], x0+CENTRE[2])
        plt.ylim(y0-CENTRE[2], y0+CENTRE[2])
    plt.gca().invert_xaxis()
    plt.title(title, fontsize=14)                                               # plot coordinates as title
    plt.xlabel('ecliptc longitude [deg]', fontsize=18)                          # x-label RA
    plt.ylabel('ecliptic latitude [deg]', fontsize=18)                          # y-label DEC
    plt.colorbar()                                                              # draw color bar
    plt.show()                                                                  # displays plot without waiting for others
    return()
#-------------------------------------------------------------------------------#
#                                                                               # 
#   DISPLAYS IMAGE OF EVENTS FILE IN PROJECTED X,Y COORDINATES                  #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_ima_skyXY(data, NAME=None, NBINS=500, CENTRE=None, RING=None):           # 50x50 pixels as default
    plt.figure(figsize=(12,10))                                                 # create plot
    plt.hist2d(data['X'], data['Y'], [NBINS,NBINS],                             # image = 2dim histogram
                cmap ='inferno', norm=LogNorm())   
    if NAME:                                                                    # if name given
        plt.suptitle(NAME, fontsize=22)                                         # plot as supertitle
    title = str(len(data))+' counts    '
    if CENTRE:                                                                  # if CENTRE (in RA, Dec) is given
        x0,y0 = radec_to_xy(data, CENTRE)                                       # convert RA, Dec to X,Y
        x1,y1 = centroid_XY(data, [x0,y0,CENTRE[2]*3600*20])                    # get centroid within given radius (in sky pixels)
        sc = CENTRE[2] *3600*20                                                         # size of image (1°)
        l = sc/2.                                                               # length of cross = 1/2 of image
        sepa = np.sqrt(np.square(x0-x1)+np.square(y0-y1))                       # separation between centroid and nominal position 
        print('separation: ', sepa)
        if sepa < 300:                                                          # too large due to ring cut out?
            plt.plot([x0-l,x0+l],[y0,y0],'b')                                   # plot blue cross at nominal position
            plt.plot([x0,x0],[y0-l,y0+l],'b')
        plt.plot([x1-l,x1+l],[y1,y1],'r')                                       # plot red cross at centroid
        plt.plot([x1,x1],[y1-l,y1+l],'r')
        plt.xlim(x0-sc, x0+sc)                                                  # set plot limits
        plt.ylim(y0-sc, y0+sc)
        title = title+'\n blue cross: Gaia position, red cross: centroid'
        #print(' deviation: ',np.sqrt((x2-x0)**2 + (y2-y0)**2)/20.)
        if RING:                                                                # reasonable only if CENTRE given also
            theta = np.linspace(0, 2*np.pi, 100)                                # azimuth angle array
            for i in range(len(RING)):                                          # for all given radii
                x = CENTRE[0] + RING[i]*np.cos(theta)                           # circle equation for x
                y = CENTRE[1] + RING[i]*np.sin(theta)                           # circle equation for y
                plt.plot(x, y, linewidth=3, color='r')                          # plot each circle
    plt.title(title, fontsize=18)                                               # plot coordinates as title
    plt.xlabel('X [1/20 arcsec]', fontsize=18)                                  # x-label X
    plt.ylabel('Y [1/20 arcsec]',  fontsize=18)                                 # y-label Y
    plt.colorbar()                                                              # draw color bar
    plt.show()                                                                  # displays plot without waiting for others
    return()
#-------------------------------------------------------------------------------#
#                                                                               #
#   PLOT AZIMUTHAL DISTRIBUTION OF COUNTS                                       #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_azimutXY(data, CENTRE, NAME=None):
    x0,y0 = radec_to_xy(data, CENTRE)                                           # get CENTRE position in X,YY
    x1,y1 = centroid_XY(data, [x0,y0,CENTRE[2]*3600*20])                        # get centroid within given radius (in sky pixels)
    phi = np.arctan2(data['Y']-y1, data['X']-x1)                                # convert to polar coordinates (angle only)
    a = np.histogram(phi, bins=36, range=(-np.pi,np.pi))                        # histogram of atimuthal distribution, 36 bins
    b = np.append(a[0],a[0][0])                                                 # in order to close the circle
    plt.figure(figsize=(12,5))                                                  # create plot figure
    ax = plt.axes(polar=True)                                                   # this is a polar plot
    ax.plot(a[1], b)                                                            # plot histogram
    ax.errorbar(a[1], b, yerr=np.sqrt(b), capsize=0, color='b')                 # add poisson error
    title = 'Azimuthal Distribution'
    if NAME: title = NAME + ': ' + title
    plt.title(title, fontsize=18)
    plt.show()                                                                  # displays plot without waiting for others
    return()
#-------------------------------------------------------------------------------#
#                                                                               # 
#   DISPLAYS IMAGE OF EVENTS FILE IN DETECTOR COORDINATES X, yield              #                   
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_ima_detXY(data, NBINS=384, NAME=None):                                             # 384 detector pixels as default
    plt.figure(figsize=(12,10))                                                 # create plot
    print('Det IMAGE: ',len(data))                                              # assumes quadratic image!
    plt.hist2d(data['RAWX'], data['RAWY'], [NBINS,NBINS],                       # image = 2dim histogram
                range=([0,383],[0,383]), cmap ='viridis', norm=LogNorm())   
    plt.xlabel('raw x', fontsize=18)                                            # x-label X
    plt.ylabel('raw y', fontsize=18)                                            # y-label Y
    title= 'Detector Image'                                                     # basic title
    if NAME:
        title = NAME + ': ' + title                                             # add name to title
    plt.title(title, fontsize=22)
    plt.colorbar()                                                              # draw color bar
    plt.show()                                                                  # displays plot without waiting for others
    return()
#---------------------------------------------------------------------------#
#                                                                           #
#   AUXILIARY ROUTINE FOR BACKGROUND COUNTS TO BE SUBTRACTED                #
#       WITHIN ENERGY RANGE, DETECTOR AREA, AND EXPOSURE TIME               #
#                                                                           #
#---------------------------------------------------------------------------#
#---------------------------------------------------------------------------#
#                                                                           #
#   BACKGROUND COUNTS TO BE SUBTRACTED                                      #
#       WITHIN ENERGY RANGE, DETECTOR AREA, AND EXPOSURE TIME               #
#                                                                           #
#---------------------------------------------------------------------------#
def background(ergrange, area, expos):                                      # energy range, detector area, exposure time
    bkgfile = 'Background//TM1_FWC_AllPat_spectra.fits'                     # background file for TM1(hard coded)
    rmffile = 'Background/TM1_FWC_AllPat_rmf.fits'                          # rmf file for TM1 (hard coded)
    with fits.open(bkgfile, memmap=False) as hdu:
        bkg = hdu[1].data                                                   # open extension 1 (= spectrum)   
        gti = hdu[2].data                                                   # open extension 2 (= gti)   
    chan = bkg['CHANNEL'];      cnts = bkg['COUNTS']                        # measured background spectrum within 1.0° FoV
    cnts[205:220] = (cnts[180:195] + cnts[220:235])/2.                      # simply kill aluminium fluorescence line of CFW
    GTI = np.int32(np.sum(gti['STOP'] - gti['START']))                      # exposure time is sum of all gti's  
    FoV = 384*384 / 1.064                                                   # This is 1° area [detector pixels] (6% less than sqare)   
    a_ratio = area / FoV                                                    # ratio between affected det. area and total 1°
    t_ratio = expos / GTI                                                   # ratio between obs time and gti in bkgr spectrum file
    with fits.open(rmffile, memmap=False) as hdu:                           # rmf needed for conversion between chan and energy
        rmf = hdu[1].data                                                   # open extension 1 (only ENERG_LO, -HI needed)
    ergbin = (rmf['ENERG_LO'] + rmf['ENERG_HI'])/2. * 1024.                 # get energy as arithm. mean, conv. eV to channel
    print(ergrange, len(ergbin))
    index = np.ones(len(ergbin))                                            # as usual, create an array with ones
    index = index * (ergbin < ergrange[1])                                  # set everything above upper energy to zero
    index = index * (ergbin > ergrange[0])                                  # set everything below loer energy to zero
    cnts  = cnts[index.nonzero()]                                           # limit COUNTS array to this range
    chan  = chan[index.nonzero()]                                           # same with CHANNEL array
    summe = np.sum(cnts)                                                    # total number of background counts
    bkgr_counts = np.int(summe * a_ratio * t_ratio + 0.5)                   # part of  within det area and obs. time
    print('BACKGROUND: ', bkgr_counts)
    return(bkgr_counts)
#-------------------------------------------------------------------------------#
#                                                                               #
#   CALCULATE VIGNETTING                                                        #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_vignet(data, NAME=None):
    rings = [0, 24, 48, 72, 96, 120, 144, 168]                                  # 8 concentric detector rings
    vign     = [0, 0, 0, 0, 0, 0, 0, 0]                                         # vignetting 
    counts   = [0, 0, 0, 0, 0, 0, 0, 0]                                         # counts in rings
    exposure = [0, 0, 0, 0, 0, 0, 0, 0]                                         # exposure tiem of rings
    ringarea = [0, 0, 0, 0, 0, 0, 0, 0]                                         # area of rings
    cntrate  = [0, 0, 0, 0, 0, 0, 0, 0]                                         # maximum count rate
    emin     = [0, 0, 0, 0, 0, 0, 0, 0]                                         # lower limit of energy range
    emax     = [0, 0, 0, 0, 0, 0, 0, 0]                                         # upper limit of energy range
    bkgr     = [0, 0, 0, 0, 0, 0, 0, 0]                                         # background counts
    error    = [0, 0, 0, 0, 0, 0, 0, 0]                                         # combined error
#------------------------------------------------------------------------------- calculate vignetting curve in 8 rings
#    evt_ima_detXY(data)                                                         # display full image in detector coords.
    for i in range(8):                          
        detring = [rings[i], rings[i]+23]                                       # each ring with 24 pixels width
        ring_data = evt_select(data, DETRING=detring)                           # select data within ring
        emin[i] = np.min(ring_data['PI'])                                        # lower limit energy
        emax[i] = np.max(ring_data['PI'])                                       # upper limit energy
        lc = evt_Lightcurve(ring_data, TIMERES=1.)                              # lightcurve
        cntrate[i] = np.max(lc)                                                 # maximum countrate taken from lightcurve
        counts[i] = len(ring_data)                                              # number of counts in ring
        TI = ring_data['TIME'] 
        obslen = np.int64(np.max(TI) - np.min(TI))                              # gross duration of observation
        obs = np.histogram(TI, bins=obslen)                                     # get all 1-second time intervals
        index = obs[0].nonzero()                                                # get all intervals with counts with counts
        exposure[i] = len(index[0])                                             # this is then the net exposure time
        ringarea[i] = np.int((np.square(detring[1]) - 
                          np.square(detring[0])) * np.pi)                       # area of ring [pixels]
        bkgr[i] = 0#background([emin[i], emax[i]], ringarea[i], exposure[i])      # calculate background counts
        bkgr[i] = 5* bkgr[i]  ###############################   for 5 telescope modules ###########################
        vign[i] = (counts[i]-bkgr[i])/ringarea[i]                               # correct?
        error[i] = (np.sqrt(vign[i] + bkgr[i])) / ringarea[i]
#------------------------------------------------------------------------------- plot vignetting curve
    detima = np.histogram2d(data['RAWX'], data['RAWY'], bins=384)               # 
    maxpix = np.int(np.max(detima[0][:]))
    f = vign[0]
    for i in range(8): vign[i] = vign[i]/f                                      # normalise to innermost bin (= on-axis)
    plt.figure(figsize=(12,10))                                                 # create plot
    plt.errorbar(np.arange(8), vign, error)
    plt.plot(np.arange(8), vign,'o')
    plt.xlabel('off-axis angle [*24 detector pixels]', fontsize=14)
    plt.ylabel('normalised vignetting', fontsize=14)
    plt.ylim(0,1.1)
    plt.title('Vignetting: '+NAME, fontsize=22)
    plt.show()                                                                  # displays plot without waiting for others
#------------------------------------------------------------------------------- print information    
    with open('ero.txt','a') as f:                                             # open log file
        f.write('\n\nVignetting: '+NAME+'\nenergy    area   ctss    gti  cnts   bkgr  vign\n')
        f.write('Maximum counts in one pixel: '+str(maxpix)+'\n')
        print('\nVignetting: '+NAME+'\n    energy      area   ctss    gti  cnts   bkgr  vign')   
        for i in range(8):                                                      # print on screen and log file
            f.write('{:4.2f} {:4.2f} {:>5d} {:>5.1f} {:>5d} {:>5d} {:>5d}   {:>4.2f} \n'.
              format(emin[i],emax[i],ringarea[i],cntrate[i],exposure[i],counts[i],bkgr[i],vign[i]))
            print('{:4.2f} {:4.2f} {:>5d} {:>5.1f} {:>5d} {:>5d} {:>5d}   {:>4.2f}'.
              format(emin[i],emax[i],ringarea[i],cntrate[i],exposure[i],counts[i],bkgr[i],vign[i]))
        print('Maximum counts in one pixel: ', maxpix)
    return(vign)
#-------------------------------------------------------------------------------#
#                                                                               #
#   POINT SPREAD Function                                                       #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_psf(data, NAME=None, CENTRE=[0,0,1000], PLOT=False, COLOR=(0.,1.,1.)):
    if PLOT: plt.figure(figsize=(12,10))                                                 # create plot
    dist = np.sqrt(np.square(data['X'])+np.square(data['Y']))
    n =37                                                                       # number of bins
    radii = 10**(np.arange(n)/12.)*22.                                          # 1arcsec - 1000 arcsec
    area = np.pi*(radii[1:]**2 - radii[:-1]**2)                                 # area of all annuli 
    ring = np.sqrt(radii[:-1]**2 + area/2./np.pi)                               # centre radii of annuli
    area = np.append([np.pi*radii[0]**2],[area])                                # add innermost circle
    ring = np.append([0.], ring)                                                # add zero for innermost circle
#-------------------------------------------------------------------------------
    counts = np.zeros(n)                                                        # create arrea of counts per annulus-bin
    counts[0] = np.sum(dist<radii[0])                                           # innermost circle
    for i in range(1, n):                                                       # all annuli
        counts[i] = np.sum(dist<radii[i]) - np.sum(dist<radii[i-1])             # counts in ring
    error = np.sqrt(counts)                                                     # poisson error  
    S = np.sum(counts[0]/area[0])                                                       # normalisation factor on innermost ring
    plt.xlim(1.2, 1300.);   plt.ylim(5.E-6, 2.E0)
    #plt.xlim(1.2, 50);   plt.ylim(5.E-6, 2.E0)
    plt.yscale('log');          plt.xscale('log')                               # log-log plot
    x = ring/20.
    y = counts/area/S
    plt.errorbar(x, y, yerr=error/area/S, color=COLOR)
    if NAME:
        plt.title('Point Spread Function, '+NAME, fontsize=18)
    else:
       plt.title('Point Spread Function', fontsize=18)
    plt.xlabel('radial distance [arcsec]', fontsize=18)
    plt.ylabel('normalised surface brightness', fontsize=18)
    if PLOT: plt.show()                                                                  # displays plot without waiting for others
    return(x,y,area)
#-------------------------------------------------------------------------------#
#                                                                               #
#   HALF ENERGY WIDTH                                                           #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_hew(data, ERGRANGE=None, NAME=None, CENTRE=[0,0,3000]):
    plt.figure(figsize=(12,10))                                                 # create plot
    n = CENTRE[2]                                                               # radius in sky coordinates
    x, y = centroid_XY(data, CENTRE)                                            # do centroiding
    dist = np.sqrt(np.square(data['X']-x)+np.square(data['Y']-y))
    radius =  np.arange(n)                                                      # define linear radius array       
    print(x,y,dist)
#-------------------------------------------------------------------------------
    counts = np.zeros(n)                                                        # create array of counts
    for i in range(n):                                                          # all annuli
        counts[i] = np.sum(dist<radius[i])                                      # counts in increasing circles 
    error = np.sqrt(counts)                                                     # poisson error
    full = counts[n-1]                                                          # total number of counts in psf  
    counts = counts / full; error = error / full                                # normalize curve   
    print('HEW: ', full)
    half = np.max(radius[counts<0.5])                                           # radius at which half of counts are within
    plt.errorbar(radius[:n]/20., counts[:n], yerr=error[:n])                    # /20 in order to get it in arcsec
    plt.plot([1,half/20],[0.5,0.5],'b')
    plt.plot([half/20.,half/20.],[0.,0.5],'b')
    if NAME:
        plt.title('Half Energy Width, '+NAME, fontsize=14)
    else:
        plt.title('Half Energy Width', fontsize=14)
    plt.xlabel('radial distance [arcsec]', fontsize=14)
    plt.ylabel('Encircled Energy', fontsize=14)
    plt.text(1., 0.75, 'HEW = {0:.1f} arcsec'.format(2.*half/20.), fontsize=18)
    plt.show()                                                                  # displays plot without waiting for others
    return()
#-------------------------------------------------------------------------------#
#                                                                               #
#   COUNTS EVENTS IN GIVEN RINGS                                                #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_in_rings(data, centre, radius):
    x,y, dist = centroid_XY(data, centre)
    counts = np.zeros(len(radius))
    area = np.zeros(len(radius))
    for i in range(len(radius)):
        counts[i] = np.sum(dist<radius[i])
    for i in range(len(radius)-1, 0, -1):
        counts[i] = counts[i]- counts[i-1]
        area[i] = (radius[i]**2 - radius[i-1]**2) * np.pi
    area[0] = radius[0]**2 * np.pi
    r = np.asarray(radius) / 20. / 60.
    area = area / 400. / 3600.
    print(('RINGS: radii of rings [skypixel]  = '+'{:10d}'*5).format(*np.int64(radius)))
    print(('RINGS: radii of rings [arcmin]    = '+'{:10.1f}'*5).format(*r))
    print(('RINGS: area of rings  [arcmin^2]  = '+'{:10d}'*5).format(*np.int64(area)))
    print(('RINGS: counts in rings            = '+'{:10d}'*5).format(*np.int64(counts)))
    
    #print(*('{}: {}'.format(*k) for k in enumerate(lst)), sep="\n")
    return()
#-------------------------------------------------------------------------------#
#                                                                               #
#   ATTITUDE JITTER CHECK                                                       #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_att(data, STEP=100, CENTRE=[0,0,400]):
    plt.figure(figsize=(12,10))                                                 # create plot
    x0, y0 = centroid_XY(data, CENTRE)                                          # do overall centroiding first
    x = data['X'];  y = data['Y']
    dist = np.sqrt( np.square(x-x0) + np.square(y-y0) )                         # distance from zero, zero
#------------------------------------------------------------------------------- CENTROIDING
    n = len(x)      
    print('Events for Attitude Jitter: ', n)
    m = np.int64(n/STEP)+1                                                      # number of groups
    x0=np.zeros(m); y0=np.zeros(m); xerr=np.zeros(m); yerr=np.zeros(m)          # create arreas of centroids
    j = 0                                                                       # start index
    for i in range(0, n-STEP, STEP):                                            # STEP through photons
        x0[j] = np.mean(x[i:i+STEP-1]);     y0[j] = np.mean(y[i:i+STEP-1])      # centroid x, y
        xerr[j] = np.sqrt(STEP);            yerr[j] = np.sqrt(STEP)             # poisson errors (WRONG!!!!)
        j = j+1                                                                 # increment index
    k = np.arange(m)                                                            # create array for abszissa
#    plt.errorbar(k+0.02, x0/20., yerr=xerr/20.)
#    plt.errorbar(k-0.02, y0/20., yerr=yerr/20.)
    plt.plot(k, x0/20., ds='steps')                                                         # plot jitter in x
    plt.plot(k, y0/20., ds='steps')                                                         # plot jitter in y
    plt.xlabel('group of photons', fontsize=14)
    plt.ylabel('deviation from overall centroid [arcsec]', fontsize=14)
    plt.title('Attitude Jitter', fontsize=14)
    ypos = np.max([x0/20,y0/20])
    plt.text(2.,ypos,'{0} events per group'.format(STEP), fontsize=14)
    return() 
#-------------------------------------------------------------------------------#
#                                                                               # 
#   PULSAR ROUTINES TAKEN FROM STINGRAY                                         #
#   PERIOD SEARCH ACCORDING TO Z^2 STATISTICS (BUCCHERI ET AL:, 1983)           #
#                                                                               #
#-------------------------------------------------------------------------------#   
def evt_Powerspectrum(data, time_res, INTERVAL=None):
        T = data['TIME']                                                        # get event times
        if (INTERVAL):                                                          # if time interval is given
            T = T[INTERVAL[0]:INTERVAL[1]]                                      # limit event times to interval
        lc = Lightcurve.make_lightcurve(T, time_res)                            # calculate lightcurve
        ps = Powerspectrum(lc)                                                  # calculate powerspectrum
        plt.plot(ps.freq, ps.power, lw=2, color='blue')                         # plot powerspectrum
        plt.xlabel('Frequency (Hz)')                                            # x-label
        plt.ylabel('Power (raw)"')                                              # y-label
        return(ps)
#-------------------------------------------------------------------------------#
#                                                                               # 
#   PULSAR ROUTINES TAKEN FROM STINGRAY                                         #
#   POWERSPECTRUM OF GIVEN FILE, WITH TIME RESOLUTION AND                       #
#               WITHIN TIME INTERVAT (OPTIONAL)                                 #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_ZSquaredSearch(data, period, interval=None, nharm=3, nbin=32, n_df=500, oversampling=15):
        T = data['TIME']                                                        # get event times
        events=EventList(T)                                                     # basic class for event list data
        df_min = 1./500.                                                        # frequency resolution
        print(interval,nharm,nbin,n_df,oversampling)                            # oversampling
        df = df_min / oversampling
        frequencies = np.arange(1/period - n_df * df, 1/period + n_df * df, df)
        freq, zstat = z_n_search(events.time, frequencies, nbin=nbin, nharm=nharm)
        #freq, efstat = epoch_folding_search(events.time, frequencies, nbin=nbin)
        plt.plot(freq, (zstat - nharm), label='$Z_2$ statistics')
        #plt.plot(freq, efstat - nbin + 1, color='gray', label='EF statistics', alpha=0.5)
        plt.axvline(1/period, color='r', lw=3, alpha=0.5, label='Correct frequency')
        plt.xlim([frequencies[0], frequencies[-1]])
        #plt.xlim([frequencies[900], frequencies[1100]])
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Statistics - d.o.f.')
        plt.legend()
        return()
#-------------------------------------------------------------------------------#
#                                                                               # 
#   PULSAR ROUTINES TAKEN FROM STINGRAY                                         #
#   EPOCH FOLDING                                                               #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_EpochFolding(data, period, nbin=8):                                     # bins per epoch, default = 32
    T = data['TIME']                                                            # get event times
    events=EventList(T)                                                         # basic class for event list data
    ph, profile, profile_err = fold_events(events.time, 1/period, nbin=nbin)
    _ = plot_profile(ph, profile)
    return()
#-------------------------------------------------------------------------------#
#                                                                               # 
#   TAKEN FROM STINGRAY                                                         #
#   CREATE LIGHTCURVE                                                           #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_Lightcurve(data, NAME=None, TIMERES=1, PLOT=None):                                 # time resolution, default = 20msec
    T = data['TIME']                                                            # get event times
    lc = Lightcurve.make_lightcurve(T, TIMERES)                                 # calculate lightcurve
    print('Lightcurve: ', TIMERES, len(lc))
    if PLOT:
        plt.figure(figsize=(12,10))                                                 # create plot
        plt.plot(lc.time, lc.counts, lw=2, ds='steps', color='blue')
        if NAME:
            plt.title('Lightcurve, '+NAME, fontsize=22)
        else:
                plt.title('Lightcurve', fontsize=14)
                plt.tick_params(labelsize=14)
                plt.xlabel('Time (s)', fontsize=18)
                plt.ylabel('Counts [cts/'+str(TIMERES)+'sec]', fontsize=18)
                plt.show()
    return(lc)
#-------------------------------------------------------------------------------#
#                                                                               #
#   SPECTRUM                                                                    #
#                                                                               #
#-------------------------------------------------------------------------------#
def evt_Spectrum(data, NAME=None):                                              # number of bins, default=200   
#    E = data['PI'] / 1000.                                                      # convert from eV to keV for plot    
    E = data['PI'] / 1000.                                                      # convert from eV to keV for plot    
    emin = np.min(E);  emax=np.max(E)                                           # get energy range
    bins = np.int64((emax-emin) * 200)                                          # binsize = 10eV
    hist, bins = np.histogram(E, bins=bins, range=(emin,emax))                  # numpy histogram with number of bins
    error = np.sqrt(hist)
    hist = hist / exposure(data) #/ 1.0634 / 829.4# / 3600.                               # exposure corrected and per square arcmin
    error = error / exposure(data) #/ 1.0634 / 829.4# / 3600.                             # same for error
    plt.figure(figsize=(12,10))                                                 # create plot
    #a = np.sum(hist); hist = hist/a
#    logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))       # equally space bins in logarithmic scale
#    plt.hist(E,bins=logbins, histtype='step')                                   # plot histogram of events in log. bins
    print(len(bins), len(hist), len(error))
    #plt.errorbar(bins[:-1], hist, error, 0.01, fmt='none')
    plt.plot(bins[:-1], hist, ds='steps')
    #plt.xscale('log')
    plt.yscale('log')
    plt.ylim(np.min(hist)/2.,np.max(hist)*2)
    if NAME:
        plt.title('Spectrum, '+NAME, fontsize=22)
    else:
        plt.title('Spectrum', fontsize=22)

    plt.tick_params(labelsize=14)
    plt.xlabel('Energy [keV]', fontsize=18)
    plt.ylabel('arbitrary [cts/sec/keV]', fontsize=18) #'/arcmin$^{-2}$]'
    plt.tick_params(axis='both', which='major', labelsize=18)
    plt.tick_params(axis='both', which='minor', labelsize=18)
    #plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
#    plt.xlim(1., 2.)
    plt.show()
    return(bins[:-1],hist)
#---------------------------------------------------------------------------#
#                                                                           #
#   GET EXPOSURE TIME                                                       #
#                                                                           #
#---------------------------------------------------------------------------#
def exposure(data):
    TI = data['TIME'] 
    obslen = np.int64(np.max(TI) - np.min(TI))                        
    obs = np.histogram(TI, bins=obslen)             
    index = obs[0].nonzero()                            
    expos = len(index[0])  
    print('EXPOSURE TIME: ',expos)
    return(expos)

