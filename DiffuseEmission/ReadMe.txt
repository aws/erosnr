Fitting diffuse emission with eROSITA
-------------------------------------

The starting point is a single event file containing all events from the region of
interest, filtered for flares if applicable. For the purpose of defining regions, images
are necessary, too.

1. Define region of interest and background as ds9 regions (ellipses, circles, annuli,
polygons, rectangles) in separate files.


2. Create region masks with point sources excised using MaskCreator.py, specifying source
& background names, and minimum detection likelihood from which to exclude point (or also
extended, if desired) sources.


3. Feed region masks into srctool using SourceExtractor.py or command line. Important
parameters for computational speed are xgrid and tstep (see official srctool
documentation). I recommend setting xgrid to a fraction > 1/100 of the source diameter to
reduce the computation time (xgrid is specified in units of 8" for some reason).
Requires initialization of eSASS.


4. Fit the resulting spectra with Xspec. There are three possible approaches:

   a) Traditional regrouping and background subtraction with ftools grppha. This may lead
   to loss of accuracy and resolution unless an enormous number of counts is present.

   b) Fit background first (command line or PyXspec), and then fix it and use as model
   component for fit of source, with (optional) variable normalization

   c) Fit source and background truly simultaneously, allowing all parameters to vary
   at the same time. 

==> Regarding options b) and c), at least three model components are necessary for the
modelling:

   i) Source model: Dependent on the expected source properties, e.g. tbabs*vapec (old SNR
   in CIE), tbabs*vpshock (young SNR, with NEI), tbabs*vrnei (old SNR, with recombining
   plasma), tbabs*(vapec+powerlaw) (SNR with nonthermal emission), etc.
   
   ii) Instrumental background model from FWC data: Combination of (broken) powerlaws and
   instrumental fluorescence lines, described in much detail in Yeung et
   al. (2023). Ideally, one should treat all TMs separately and use the adequate FWC model
   for each. However, I have used a "merged" model of the different TMs, assuming identical
   exposures, which can be used at your own risk.
   In practice, I found that only varying the global normalization of this background
   component was necessary, BUT I'm not sure if this applies if there are strong
   unfiltered flares in the data!

   iii) Astrophysical background model: Many options exist here, I have been using a model
   like: acx+apec+tbabs*(apec+apec+powerlaw). In the end, what matters, is that the
   background is well fit. The individual components are motivated physically:
      	acx: solar wind charge exchange (Smith et al. 2014); ATTENTION! This needs to be
      	installed beforehand (https://github.com/AtomDB/acx)
      	apec: Local hot bubble, typical temperatures ~ 0.1 keV, unabsorbed since nearby
      	apec: Galactic halo, typical temperature ~ 0.25 keV.
      	apec: Galactic "corona" (Ponti et al. 2022), or unresolved point sources in the
   disk, typically >~ 0.5 keV
      	powerlaw: Cosmic X-ray background. I have been using the fixed normalization and
   slope (\Gamma=1.46) of De Luca & Molendi (2004). However, there are indications that
   their normalization is overestimated, as for instance Ponti et al. (2022) and Yeung et
   al. (2023) find lower normalizations.
   
	==> It is not a priori clear which parameters have to be fixed and which ones can be
   left free here (e.g. Galactic absorption, LHB temperature, CXB normalization, halo
   metallicity, ...). I recommend trying until the background fit looks good, bonus points if
   parameters are physically reasonable.


   ==> An example for loading spectra, responses and models in command-line Xspec could
   look like this (an equivalent example which can be executed in Xspec is given in
   ExampleFitWithBackground.xcm):
       "data 1:1 Source.fits 2:2 Background.fits" (Load source and background spectra,
       indicating that they will get different models) 
       "response 1:1 RMF.fits 2:1 RMF.fits 3:1 RMF.fits" (Define responses for three model
       components toward source spectrum)
       "arf 1:1 ARF.fits 3:1 ARF.fits" (do not apply ARF to model 2, which will be
       instrumental background)
       "response 2:2 RMF.fits 3:2 RMF.fits" (Define responses for background spectrum, no
       response toward source model)
       "arf 3:2 ARF_Back.fits" (Define ARF for background spectrum, towards astrophysical
       background; should be different from source ARF, as region size may be vastly
       different)
       "ignore 1:**-0.2 8.5-** 2:**-0.2 8.5-**" (Ignore channels outside 0.2-8.5 keV for
       both spectra)
       "model 1:src tbabs*vpshock" (Define source model, naming it "src")
       "model 2:pback const(powerlaw+powerlaw+...)" (Define instrumental background model)
       "model 3:xback const(acx+apec+tbabs*(apec+apec+powerlaw))" (Define X-ray background
       model)
       "newpar src:1 0.1" (Example for setting a parameter, N_H in this case)

       ==> The "const" allows for different normalizations between the source & background
       regions; the FITS keyword BACKSCAL/REGAREA specifies how the particle/X-ray
       backgrounds should scale between the two, respectively.


   ==> An example "spectral fitting pipeline", which loads, fits, and plots source &
   background spectra in PyXspec with physically motivated models is provided in the file
   PyXspec_SourceBackground.py. Let me know what bugs you find!

       
       
       
       

   
      
   
