#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 15:25:20 2019

@author: mmayer
"""

import matplotlib.pyplot as plt
from astropy.io import fits #import packages
import numpy as np
from astropy.visualization import astropy_mpl_style
import xspec
from xspec import *
import os
import pickle
import pandas as pd

### Plotting style ###
plt.style.use('classic')    
plt.style.use(astropy_mpl_style)  
plt.rcParams['font.family']='serif'
plt.rcParams["axes.edgecolor"] = "black"
plt.rcParams["axes.linewidth"] = 2.5



""" 
    This program allows for fitting, and plotting source & background spetra simultaneously. 
    In the following section, important parameters are set. Then the function is defined and called, and a plot is created. 
"""


""" Set the parameters/file names: 
    --> Example spectrum file: Test020_SourceSpec_00001_TestPolygon_mask.fits
"""

### directory, file details
Dir = 'srctool_products/Vela' # Directory of the spectra 
prefix = 'Test' ## Prefix of spectrum name
specname = '_TestPolygon_mask' ### Optional suffix to file name, after processing, e.g. "_grp"
backname = '_Back_mask' ## Extra prefix of background spectra (spec --> backspec)
#version = 'c020' ## Processing
version = '' 
TM = 0 ### TM number (0 for merged telescope spectrum (8 is also ok))

### Construct file path 
Path = '%s%i20_' % (prefix,TM)

### model, save name...
Srcmodel = 'tbabs*vpshock' # Model to be fitted. Cannot use arbitrary model, but should be model that is implemented further up! 
Save = 'SimFit_vpshock' # Name of plot to be saved
##################################################################################


### Energies for fitting
emin = 0.2
emax = 8.5
### Energies for plotting
eminplot = 0.2
emaxplot = 4.0#8.5#
rebin_plot = 5. ### S/N of bins for plotting!
grid_plot = False ### Whether to include a background grid in plot

Err = False # Whether to get error bars on parameters
MCMC = False # Whether to get constraints on parameters via MCMC

""" Background parameters & models """
### Particle background model (illegally TM-merged FWC model from Mike)
pback = 'constant(bknpower+powerlaw+gaussian+gaussian+gaussian+gaussian+gaussian+gaussian+gaussian+gaussian+gaussian+gaussian+gaussian)'
BckgPars=[1., ### NORMED PER SQUARE DEGREE FOR 5 TMs!
          0.108458,6.12355,0.441352,1.15224, ###bknpower ==> Basic underlying model
          1.69259,0.0608137, ### PL2
          0.28513,0.0607209,0.0366923, ### Fake "line" for low-E curvature
          1.65148,0.0648686,-0.0149536, ### Negative line around Al Kalpha 
          1.91497,0.00032481,0.0104798, ### Positive line around Al Kalpha
          8.62185,0.0492488,0.0351143, ### Line 1
          8.09676,0.119405,0.022284, ### Line 2
          7.48154,0.011814,0.0255957, ### Line 3
          7.02976,0.130612,0.0231355, ### Line 4
          6.41715,0.0397597,0.0660927, ### Line 5
          5.91499,2.59619e-05,0.00516779, ### Line 6
          3.68671,0.0881975,0.0162009, ### Line 7                
          1.48662,8.28839e-06,0.0582942] ### Line 8

### XRB model
xrb = 'apec+acx+TBabs(apec+apec+powerlaw)'
xrb_pars = [0.146473,1.,0.,1.71999e-06, ### LHB 
             0.0940947,0.0909,1.,0.,1,8,2.97804e-08, ### ACX: SWCX EMISSION!
             0.55351, ### Galactic N_H
             0.169315,1.,0.,6.74433e-05, ### GH APEC --> To adjust for line strength!
             0.407826,1.,0.,3.03316e-07, ### CORONA APEC --> To adjust for line strength!
             1.46,0.98E-6, ### CXB NORMALIZATION 
             ]
xrb_Frozen = [False, True, True, False, 
              False, True, True, True, True, True, False,
              False,
              False, True, True, False,
              False, True, True, False,
              True, False]


""" Define Parameters & limits according to requested source model 
    --> Some examples for common models """
    
if Srcmodel == 'tbabs*pshock':
    Pars = [0.03,0.1,1.0,0.0,1E12,0.0,0.16]### Set source spectrum parameters 
    Lows = [0,0.01,0.1,0.0,1E9,0,1E-3]### Lower limits
    Highs = [1E1,0.3,10.,0.0,1E14,0,1E3]### Upper limits
    Frozen = [False, False, False, True, False, True, False] ### Which parameters to freeze
    
elif Srcmodel == 'tbabs*apec':
    Pars = [0.03,0.1,1.0,0.0,0.16] ### Set source spectrum parameters 
    Lows = [0,0.01,0.1,0,1E-3] ### Lower limits 
    Highs = [1E1,1.0,10.0,0,1E3] ### Upper limits
    Frozen = [False, False, False, True, False]
    
elif Srcmodel == 'tbabs*vpshock':
    Pars = [0.05,0.55,1.0,1.0,1.0,1.0,0.6,0.9,0.6,0.8,0.7,1.0,1.0,0.4,1.0,0.0,1.8E11,0.0,11.]### Set source spectrum parameters 
    Lows = [0,0.1,1.0,1.0,5E-2,5E-2,5E-2,5E-2,5E-2,5E-2,5E-2,1.0,1.0,5E-2,1.0,0.0,1E9,-0.05,1E-3] ### Lower limits
    Highs = [1E1,3.0,1.0,1.0,2E1,2E1,2E1,2E1,2E1,2E1,2E1,1.0,1.0,2E1,1.0,0.0,1E14,0.05,1E5]### Upper limits
    Frozen = [False, False, True, True, True, False, False, False, False, False, False, True, True, False, True, True, False, True, False]
    #        [nH,    kT,    H,    He,   C,    N,    O,     Ne,    Mg,    Si,    S,     Ar,   Ca,   Fe,    Ni,   tau_l,tau_h, z,    norm  ]

elif Srcmodel == 'tbabs*vapec':
     Pars = [0.03,0.20,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.0,7.2]
     Lows = [0.,0.05,1.0,5E-2,5E-2,5E-2,5E-2,5E-2,1.0,5E-2,1.0,1.0,1.0,5E-2,1.0,0.0,1E-3]
     Highs = [3E-1,0.4,1.0,2E1,2E1,2E1,2E1,2E1,1.0,2E1,1.0,1.0,1.0,2E1,1.0,0.0,1E5,]
     Frozen = [False, False, True, True, False, False, False, False, True, False, False, True, True, False, True, True, False]
#             [nH,    kT,    He,   C,    N,     O,     Ne,   Mg,     Al,   Si,    S,     Ar,   Ca,   Fe,    Ni,   z,    norm  ]
### Could use e.g. getattr(getattr(m, 'apec'), 'kT').values for accessing parameters using a dictionary, but a simple list seems easier

""" Functions for spectral fitting """

def BkgSimFits(Dir,Path,Version,specname,Srcmodel,backname='',
               Pars=[],Lows=[],Highs=[],Frozen=[],
               Err=False, MCMC=False,
               link_pnorm=True, link_xnorm=True, 
               Bckgmodel='',Bckgmodel2=[],BckgPars=[],BckgPars2=[],BckgFrozen2=[],
               emin=0.2,emax=8.5,rebin_plot=5., 
               ### MCMC parameters
               priors = [], N_Burn = 1000, N_Split = 1, N_Sample = 1000,N_Walkers_Burnin = 100, N_Walkers_Sample = None, 
               relscale = 0.1,parallel=False, max_cores=16, 
               ): 
    
    """ 
    This function performs the "hard work", the actual fitting, including the loading of spectra, 
    setting up of parameters & limits and extraction of results. 
    
    The main parameters are:
        - Dir: The directory where the spectral files are
        - Path: The prefix of the files (before "SourceSpec")
        - Version: Processing number (if part of the filenames)
        - specname, backname: Suffixes of the source / background file names;
          if backname == '', script will assume it was given the automatic background name by srctool
        - Srcmodel: Xspec model expression to fit to the source
        - Pars, Lows, Highs: Parameter initial values, lower & upper limits, supplied as lists
        - Frozen: Which model parameters to freeze
        - Err: Whether to extract parameter errors after fitting (May take a LONG time)
        - MCMC: Whether to run emcee after simple fit to extract parameter samples
        - link_pnorm, link_xnorm: Whether to link the relative normalizations of particle / X-ray background models between source and background regions
        - Bckgmodel, Bckgmodel2: Xspec model expressions of instrumental/X-ray background models
        - BckgPars, BckgPars2: Parameters of instrumental/X-ray bacground models, supplied as lists
        - BckgFrozen2: Which parameters of X-ray background model to freeze
        - emin, emax: Energy range to include in the fitting
        - rebin_plot: S/N ratio, to which the spectra will be binned for plotting
        
        --> remaining parameters are passed to MCMC if called, see function run_mcmc()
    """
    
    """ Set up XSpec parameters """
    if MCMC:
        global AllModels, AllData, Fit, Plot
    
    xspec.AllModels.clear()
    AllModels = xspec.AllModels
    AllData = xspec.AllData
    AllData.clear()
    AllModels.lmod('acx','~/data1/Data/ACX_Xspec/acx/xspec')
#    AllModels.lmod('acx','./acx_xspec') # --> Path to acx model!
    
    Xset.abund='wilm'
    Xset.xsect='vern'
    Xset.parallel.error = 8    
    Xset.parallel.steppar = 8
    
    Fit.statMethod = "cstat"   # C statitics
    Fit.query = 'yes'   
    Fit.nIterations = 100
    Fit.criticalDelta = 1E-2
    
    
    """ Load spectra and responses """
    
    ### Go to the directory of spectrum files, to avoid errors when spectra want to load their own response ###
    CurrentDir = os.getcwd()
    if Dir[0] == '/': ###absolute path
        os.chdir(Dir)        
    elif '~' in Dir: ### expand to relative path
        Dir = os.path.expanduser(Dir)
        os.chdir(Dir)
    else: ### relative path
        os.chdir('%s/%s' % (CurrentDir,Dir))

    ### Construct paths of spectra, RMFs, ARFs from keywords
    fullpath = '%sSourceSpec_00001%s%s.fits' % (Path,Version,specname)
    arfpath = '%sARF_00001%s%s.fits' % (Path,Version,specname)
    rmfpath = '%sRMF_00001%s%s.fits' % (Path,Version,specname)

    if backname == '': ### DIFFERENT THAN OTHER CASES DUE TO NAMING PROBLEMS!
        bpath = '%sBackgrSpec_00001%s%s.fits' % (Path,Version,specname)
        barfpath = arfpath
        brmfpath = rmfpath
    else:
        bpath = '%sSourceSpec_00001%s%s.fits' % (Path,Version,backname)
        barfpath = '%sARF_00001%s%s.fits' % (Path,Version,backname)
        brmfpath = '%sRMF_00001%s%s.fits' % (Path,Version,backname)
    

    ### Get scaling factors for source/background!
    SFits = fits.open(fullpath)
    SHead = SFits[1].header  
    BFits = fits.open(bpath)
    BHead = BFits[1].header  
    
    try:
        SScal = SHead['REGAREA']
        BScal = BHead['REGAREA']
        SPScal = SHead['BACKSCAL']
        BPScal = BHead['BACKSCAL']
    except:
        SExp = SHead['EXPOSURE']
        BExp = BHead['EXPOSURE']
        SScal = SHead['BACKSCAL']*SExp
        BScal = BHead['BACKSCAL']*BExp
        SPScal = SHead['BACKSCAL']
        BPScal = BHead['BACKSCAL']
        
    SFits.close()
    BFits.close()

    backfactor = BScal/SScal ### This is the relative scaling factor for X-ray background! 
    pbackfactor = BPScal/SPScal ### This is the relative scaling factor for non X-ray background! 
    
    print('Background scaling factors:',backfactor,pbackfactor)

    ### Load source & background spectra
    DataString = "%i:%i %s " % (1,1,fullpath)
    DataString += "%i:%i %s " % (2,2,bpath)
        
    try:
        AllData(DataString) ### Actual loading command
        
        ### Load responses and notice correct ranges###        
        s = AllData(1)
        s.background=None
        s.ignore('**-%s  %s-**' % (str(emin),str(emax)))
        
        b = AllData(2)
        b.background=None
        b.ignore('**-%s  %s-**' % (str(emin),str(emax)))
        
        ### DONT WANT BACKGROUND TO REACT TO SOURCE COMPONENT!
        b.multiresponse[0] = None
        
        s.multiresponse[1] = rmfpath
        b.multiresponse[1] = brmfpath
        s.multiresponse[1].arf = None ### PARTICLE BACKGROUND ==> NO ARF ###
        b.multiresponse[1].arf = None
        if len(Bckgmodel2): ### X-ray background
            s.multiresponse[2] = rmfpath
            b.multiresponse[2] = brmfpath
            s.multiresponse[2].arf = arfpath
            b.multiresponse[2].arf = barfpath
    
    
    except Exception as e: ### Make sure we go back to original directory if error!
        print(e)
        print('Aborting!')
        os.chdir(CurrentDir)
        return
                        
    os.chdir(CurrentDir)

    AllData.show()

    """ Now, all data is loaded. I start defining my models. """
        
    AllModels += (Srcmodel, "src", 1)
    
#    AllModels += ('const(%s)' % Bckgmodel, "pback", 2)
    AllModels += (Bckgmodel, "pback", 2)
    
    AllModels += ('const(%s)' % Bckgmodel2, "xback", 3) #--> introduce constant to allow for varying normalizations
            
    ### Setting model parameters and freezing/linking 
    m = AllModels(1,'src')
    m_b_s = AllModels(1,'pback')
    m_b_b = AllModels(2,'pback')

                    
    """ Particle Background """
    for i in range(m_b_s.nParameters):
        bspar = m_b_s(i+1)
        bbpar = m_b_b(i+1)
            
        if i == 0: ### Set the fixed relative scaling factor for particle background, don't allow anything else to vary!
            bspar.values = SPScal
            bspar.frozen=False            
            bbpar.values = BPScal# pbackfactor
            bbpar.frozen=False    

            if link_pnorm: ### whether to link particle background normalizations
                bbpar.link = "pback:%i * %.6f" % (i+1, pbackfactor)
            else:
                bbpar.values = "%.6f,,%.6f,%.6f,%.6f,%.6f" % (BPScal,BPScal/10.,BPScal/10.,BPScal*10.,BPScal*10.) 
                
        else:
            if BckgPars[i] >= 0:                
                bspar.values = BckgPars[i]
            else:                
                bspar.values = "%.10f,,%.10f,%.10f,," % (BckgPars[i], 2*BckgPars[i], 2*BckgPars[i])
            bbpar.link = "pback:%i" % (i+1)
            bspar.frozen=True
                
                
    """ Set X-ray background """
    ### Here, different models could be applicable
    m_b2_s = AllModels(1,'xback')
    m_b2_b = AllModels(2,'xback')
    for i in range(m_b2_s.nParameters):
        bspar = m_b2_s(i+1)
        bbpar = m_b2_b(i+1)

        if i == 0: # Set the fixed relative scaling factor for X-ray background
            bspar.values = 3600.*SScal #Get correct normalization, use surface brightness per arcmin²
            bspar.frozen=True            
            bbpar.values = 3600.*BScal #Get correct normalization, use surface brightness per arcmin²
            if link_xnorm:
                bbpar.frozen=True    
            else:
                bbpar.frozen=False ### Allow normalizations to vary independently!    
                bbpar.values = "%.3f,,%.3f,%.3f,%.3f,%.3f" % (3600.*BScal,360.*BScal,360.*BScal,36000.*BScal,36000.*BScal) 
            
            
        else: ### Set sensible limits on all temperatures, photon indices, nH, norms
            if 'PhoIndex' in bspar.name:
                l,h = (-2+BckgPars2[i-1],2+BckgPars2[i-1]) ### photon indices
            elif 'kT' in bspar.name: 
                l,h = (1/2*BckgPars2[i-1],2*BckgPars2[i-1]) ### temperatures
            elif 'nH' in bspar.name: 
                l,h = (0,10.) ### Absorption in [0, 1E23]
            elif 'norm' in bspar.name:
                l,h = (1E-4*BckgPars2[i-1],1E+4*BckgPars2[i-1]) ### norms
            else: 
                l,h = None, None 
                
            
            if h:   
                bspar.values = "%.10f,,%.15f,%.15f,%.15f,%.15f" % (BckgPars2[i-1],l,l,h,h) 
            else:   
                bspar.values = BckgPars2[i-1]
            
            ### Link background and source region parameters
            bbpar.link = "xback:%i" % (i+1)
            ### Freeze parameters for which desired!
            bspar.frozen = BckgFrozen2[i-1]
            

        
    """ Finally, set source model! """
    ### Set parameters!
    for i in range(m.nParameters):        
        par = m(i+1)
        p = Pars[i]
        l = Lows[i]
        h = Highs[i]
        par.values = "%s,,%s,%s,%s,%s" % (str(p),str(l),str(l),str(h),str(h))
        
        par.frozen = Frozen[i]

    """ Execute fit & potentially errors & MCMC! """
    AllModels.show()
    Fit.perform()
    
    if Err:
        Fit.error('1.0 src:1-%i' % (m.nParameters))
          
    if MCMC:
        res = run_mcmc(Dir=Dir, priors = priors, N_Burn = N_Burn, N_Split = N_Split, N_Sample = N_Sample, 
                       N_Walkers_Burnin = N_Walkers_Burnin, N_Walkers_Sample = N_Walkers_Sample, relscale=relscale, 
                       parallel=parallel, max_cores=max_cores)
        return res
    
    """ Fit is done. Now extract parameters and plotting data... """
    Fit.show()
    
    CSTAT = Fit.statistic  ### Goodness of fit  

    ### Plot parameters  
    Plot.device = "/null"
    Plot.xAxis = "keV"
    Plot.add = True
    
    ### Rebin the data for displaying!
    Plot.setRebin(rebin_plot, 1000, groupNum=-1)

    Plot('ldata delchi')


    ### Gather data & model parameters to plot!
    xdata = Plot.x(1,1)
    xErrs = Plot.xErr(1,1)
    yErrs = Plot.yErr(1,1)
    ydata = Plot.y(1,1)
    yratioErr = Plot.yErr(1,2)
    yratio = Plot.y(1,2)
    modvalues = Plot.model(1,1)
    Bxdata = Plot.x(2,1)
    BxErrs = Plot.xErr(2,1)
    ### Rescaling Background to source level
    ByErrs = np.array(Plot.yErr(2,1))/pbackfactor
    Bydata = np.array(Plot.y(2,1))/pbackfactor  
    Bmodvalues = np.array(Plot.model(2,1))/pbackfactor
        
        
    ### Extract best-fit parameters (+ possibly errors) 
    ARRAY = []
    ERROR = []
    
    for i in range(m.nParameters):   
        Val = m(i+1).values
        err = m(i+1).error
        ARRAY.append(Val[0])
        ERROR.append(err[:2])
    
    
  
    
    """ SEE IF I CAN EXTRACT PURE SOURCE SPECTRUM MODEL! """ 
    m = AllModels(1,'src')
    m_b_s = AllModels(1,'pback')
    m_b2_s = AllModels(1,'xback')
    m_b_s(1).values = 0
    m_b2_s(1).values = 0
    Plot.setRebin(0 ,1000,groupNum=1)
    Plot('ldata delchi')

    Sourcemodvalues = Plot.model(1,1)
    Sourcexdata = Plot.x(1,1)


    ### PRINT SOURCE MODEL AND DUMP TO A FILE 
    if Err:
        ARRAY = np.array(ARRAY)
        ERROR = np.array(ERROR)
        ERROR[:,0] = ARRAY - ERROR[:,0]
        ERROR[:,1] = ERROR[:,1] - ARRAY
    
        file = open('%s/Results_%s.txt' % (Dir, Srcmodel.split('*')[-1]),'w') ### File name should be dependent of source model
         
        print('Source Model:')
        file.write('Source Model: \n')
        for i in range(m.nParameters):
            if not m(i+1).frozen:
                print('%s:' % m(i+1).name, ARRAY[i],'+',ERROR[i][1],'-',ERROR[i][0])
                file.write('%s: %.5f + %.5f - %.5f \n' % (m(i+1).name, ARRAY[i],ERROR[i][1],ERROR[i][0]))
        
        print('Total C Statistic:',CSTAT)
        file.write('Total C Statistic: %.2f' % CSTAT)
        file.close()
            
    else:
        ARRAY = np.array(ARRAY)
        ERROR = np.array(ERROR)
        ERROR[:,0] = ARRAY - ERROR[:,0]
        ERROR[:,1] = ERROR[:,1] - ARRAY
    
        file = open('%s/Results_NoErr_%s.txt' % (Dir, Srcmodel.split('*')[-1]),'w') ### File name should be dependent of source model
         
        print('Source Model:')
        file.write('Source Model: \n')
        for i in range(m.nParameters):
            if not m(i+1).frozen:
                print('%s:' % m(i+1).name, ARRAY[i])
                file.write('%s: %.5f\n' % (m(i+1).name, ARRAY[i]))
        
        print('Total C Statistic:',CSTAT)
        file.write('Total C Statistic: %.2f' % CSTAT)
        file.close()
            
      ### Return Plotting data as well as best-fit parameters
    return ARRAY,[xdata,xErrs,ydata,yErrs,yratio,yratioErr,modvalues,Bxdata,BxErrs,Bydata,ByErrs,Bmodvalues,Sourcexdata,Sourcemodvalues],CSTAT, ERROR


def run_mcmc(Dir, 
             priors = [], 
             N_Burn = 1000, N_Split = 1, N_Sample = 1000, 
             N_Walkers_Burnin = 100, N_Walkers_Sample = None, 
             relscale = 0.1,
             parallel=False, max_cores=16, ):
    
    """ 
    Helper function, with the goal of executing emcee calls automatically. Following parameters:
        - priors: Optional list of 'lin' or 'log' strings, specifying whether linearly or logarithmically uniform priors should be used
        - N_Burn, N_sample: Number of iterations during burn-in and sampling runs
        - N_split: If larger than 1, burn-in will be split into multiple iterations, where the worst walkers are filtered out, and new ones are spawned
        - N_Walkers_Burnin, N_walkers_sample: Number of walkers to use during burnin and sampling, by default equal
        - relscale: Relative size size (as fraction of bounds) of the Gaussian "ball" in which walkers are initialized around best fit
        - parallel: Whether to execute sampling in parallel
        - max_cores: Maximum number of cores for parallel sampling
    """        
    
    import emcee
    import numpy.random as rnd
    import warnings
    from tqdm import tqdm

    if parallel:
        import multiprocessing
        from joblib import Parallel, delayed

    ### Shut things up!
    warnings.simplefilter("ignore")
    Xset.chatter = 0
    Xset.logChatter = 0


    """ Start by determining the addresses and number of free parameters """ 
    Frozens = []
    CurrentPars = []
    ParNames = []
    Bounds = []
    Models = ['src','pback', 'xback']
    N_spec = AllData.nSpectra
    
    ### New level, need to iterate over different spectra too
    for n in range(1,N_spec+1):
        for mod in Models:
            try:
                m = AllModels(n,mod)
            except:
                Frozens.append(np.zeros((0)))
                Bounds.append(np.zeros((0,2)))
                CurrentPars.append(np.zeros((0)))
                ParNames.append(np.zeros((0)))
                continue
            froz = np.array([m(i+1).frozen for i in range(m.nParameters)])
            links = np.array([m(i+1).link != '' for i in range(m.nParameters)])
            Frozens.append(froz+links) ### Linked ones count as frozens
            
            try: ### Since some parameters dont actually have bounds!
                bounds = np.transpose([[m(i+1).values[2] for i in range(m.nParameters)], [m(i+1).values[4] for i in range(m.nParameters)]])
            except IndexError:
                bounds = []
                for i in range(m.nParameters):
                    if len(m(i+1).values) > 1:
                        bounds.append([m(i+1).values[2], m(i+1).values[4]])
                    else:
                        bounds.append([m(i+1).values[0]-1, m(i+1).values[0]+1]) ### Could likely never be fitted anyway!
                bounds = np.array(bounds)
            Bounds.append(bounds)
            
            pars = np.array([m(i+1).values[0] for i in range(m.nParameters)])
            CurrentPars.append(pars)
            
            names = np.array(['%s_%s' % (mod, m(i+1).name) for i in range(m.nParameters)])
            ParNames.append(names)
        
        
    ThawInd = [np.where(froz == False)[0] for froz in Frozens]
    IndMapper = []
    for i,ind in enumerate(ThawInd):
        for j in ind:
            IndMapper.append([i,j])
    
    IndMapper = np.array(IndMapper).astype(int) ### Maps modnr, index!
    N_Free = len(IndMapper)
    
    BestPars = np.array([])
    param_names = np.array([])
    ParBounds = []
    for n in range(1,N_spec+1):
        for j, mod in enumerate(Models):
            i = j+(n-1)*len(Models)
            Inds = (IndMapper[:,0] == i) ### Whether or not each param corresponds to this model component 
            Nums = IndMapper[:,1][Inds] ### Numbers of parameters to be set
            
            pars = CurrentPars[i][Nums]
            BestPars = np.append(BestPars,pars)
            
            names = ParNames[i][Nums]
            param_names = np.append(param_names,names)
            
            bounds = Bounds[i][Nums]
            for j in bounds:
                ParBounds.append(j)
        
    ### Fix param_names
    new_param_names = list(param_names.copy())
    for i, name in enumerate(param_names):
        if name in param_names[:i]:
            n = np.sum(param_names[:i] == name) + 1
            newname = '%s_%i' % (name, n)
            new_param_names[i] = newname
    param_names = new_param_names   
     
    ParBounds = np.array(ParBounds)
    for i in zip(param_names, ParBounds):
        print('Parameter:', i[0], 'Bounds:', i[1])
    
    """ Set up the emcee sampler, initial state, and prob function """    
    ### Start the actual MCMC procedure!
#    priors = []
#    N_Sample = 20 ### Standard length, for good sampling with 
#    N_Walkers = 100 ### Generous sampling to find minimum
#    N_Walkers_Sample = 50 #GW --> Minimum for our case!
#    N_Burn = 20
#    N_Split = 1 #5 #10 ### Number of times to split burnin process and reiterate!
#    relscale = 0.1### Vary relscale & Compare errors!
    
    N_Walkers, N_Walkers_Sample = N_Walkers_Burnin, N_Walkers_Sample if N_Walkers_Sample else N_Walkers_Burnin

    ### Define initial parameters 
    print('Setting up initial states')
    if not len(priors):
        priors = ['lin' if ('PhoIndex' in n or 'nH' in n) else 'log' for n in param_names]
    linmask = (np.array(priors) == 'lin')
    logmask = (np.array(priors) == 'log')
    print('Priors:', priors)
    
    BestPars = np.log10(BestPars+1E-19)*logmask + BestPars*linmask
    ParBounds = np.transpose(np.log10(ParBounds+1E-15).T*logmask + ParBounds.T*linmask)
    ### SHIFT TO ACCOUNT FOR NUMERICS!
    BestPars = BestPars + 1E-6*logmask*(BestPars-1E-6<ParBounds[:,0]) - 1E-6*logmask*(BestPars+1E-6>ParBounds[:,1]) ### MAKE SURE THEY ARE IN RANGE!
                
    linscales = ParBounds[:,1]-ParBounds[:,0]
    p0 = []
    
    if relscale < 1.0:
        for i in range(N_Walkers):
            while True:
                pars = BestPars + linscales*rnd.normal(loc=0.,scale=relscale,size=N_Free) ##logarithmic multiplier + linear adder!
                if np.sum((pars < ParBounds[:,0]) + (pars > ParBounds[:,1])) == 0: ### Truncate!
                    p0.append(pars)
                    break
    else:
        p0 = ParBounds[:,0] + rnd.uniform(size=(N_Walkers, len(ParBounds)))*(ParBounds[:,1]-ParBounds[:,0])                            

    p0 = np.array(p0)
                    
    print('Setting up prior')
    ### Get logratihmic norm for prior!
    log_prior_norm = np.sum(np.log((ParBounds[:,1]-ParBounds[:,0])**-1))
    def log_prior(TryPar):
        return log_prior_norm

    print('Prior Norm:',log_prior_norm)
    
    def Likelihood(TryPar):
        pardicts = []
        for n in range(1,N_spec+1):
            for j, mod in enumerate(Models):
                i = j+(n-1)*len(Models)
                Inds = (IndMapper[:,0] == i) ### Whether or not each param corresponds to this model component 
                Nums = IndMapper[:,1][Inds] ### Numbers of parameters to be set
                
                locpar = TryPar[Inds]
                dic = {int(Nums[k]+1):locpar[k] for k in range(len(Nums))}
                
                try:
                    m = AllModels(n,mod)
                except:
                    continue
                pardicts += [m, dic]
            
        AllModels.setPars(*pardicts)
        
        C = Fit.statistic
        L = - C/2
            
        return L
    
        
    global log_prob  
    def log_prob(TryPar):
        if len(TryPar) != N_Free:
            print('Disagreement',len(TryPar), N_Free)
            raise Exception 
            
        ### Bounds enforced!
        if np.sum((TryPar < ParBounds[:,0]) + (TryPar > ParBounds[:,1])) > 0: ### OUTSIDE BOUNDS!
            return -1E12
        
        p = log_prior(TryPar)
        ExpPar = 10**TryPar
        ExpPar[(ExpPar!=ExpPar) + (np.isinf(ExpPar))] = 0 ### safeguard against insane values
        NatTryPar = TryPar*linmask + ExpPar*logmask
        l = Likelihood(NatTryPar)          

        return p + l
    
    ref_prob = log_prob(BestPars)
    print('Reference prob of best-fit',ref_prob)
    
    ### Define sampler
    if parallel:
#        with multiprocessing.Pool(16) as pool:
        sampler = emcee.EnsembleSampler(N_Walkers, N_Free, log_prob, pool=multiprocessing.Pool(max_cores)) ### Goodman-Weare
        if N_Walkers != N_Walkers_Sample:
            final_sampler = emcee.EnsembleSampler(N_Walkers_Sample, N_Free, log_prob, pool=multiprocessing.Pool(max_cores)) ### Goodman-Weare
    else:
        sampler = emcee.EnsembleSampler(N_Walkers, N_Free, log_prob) ### Goodman-Weare
        if N_Walkers != N_Walkers_Sample:
            final_sampler = emcee.EnsembleSampler(N_Walkers_Sample, N_Free, log_prob) ### Goodman-Weare


    """ Start sampling: Burn-in & sampling run """
    if N_Split > 1:
        print('Burn in separated into %i steps' % N_Split)
        N_Burn = N_Burn // N_Split ### Seaprate it into multiple loops!
        
    ### Burn-in
    print('Burn in')
    state = sampler.run_mcmc(p0, N_Burn, progress=True)
    Likes = np.average(np.log(-sampler.get_log_prob(flat=False)),axis=1)
    
    ### Optional splitting of burn-in step into multiple iterations
    if N_Split > 1:
        for n in range(N_Split-1):
            ### First trim walkers organizedly!
            Walkers = state.coords
            Walker_Probs = state.log_prob
     
            Thresh = np.sort(Walker_Probs)[-(N_Walkers//2)] ### Throw away half the points!                    
            MaskWalkers = Walkers[Walker_Probs >= Thresh]
            
            
            ### Respawn a new set of walkers, following covariance matrix!
            mean, cov = np.mean(MaskWalkers,axis=0), np.cov(MaskWalkers.T)
            FullWalkers = np.zeros_like(Walkers)
            FullWalkers[:N_Walkers//2] = MaskWalkers
            
            
            for i in range(N_Walkers//2, N_Walkers):
                while True:
                    pars = rnd.multivariate_normal(mean, cov)
                    if np.sum((pars < ParBounds[:,0]) + (pars > ParBounds[:,1])) == 0: ### Truncate!
                        FullWalkers[i] = pars
                        break
                    
            ### Rerun sampling with new set of walkers!
            sampler.reset()
            state = sampler.run_mcmc(FullWalkers, N_Burn, progress=True)
            NewLikes = np.average(np.log(-sampler.get_log_prob(flat=False)),axis=1)
            Likes = np.concatenate([Likes, NewLikes]) ### Simply added to what we know!
        print('Likelihood evolution', Likes)
    
    print('Final likelihoods at end of burn-in:',-np.exp(Likes[-1]))
    print('--> Expectation from Reference prob',ref_prob-N_Free/2.)
    
    ### Trim walker sample down!
    if N_Walkers != N_Walkers_Sample:
        Max_Delta = 50.
        
        Walkers = state.coords
        Walker_Probs = state.log_prob
        
        Max_Prob = np.max(Walker_Probs)
        if np.sum(Walker_Probs > Max_Prob-Max_Delta) < N_Walkers_Sample:
            print('ATTENTION: NOT ENOUGH SAMPLING QUALITY WALKERS FOUND; USING BEST ONES! STRICTLY SPEAKING NOT CONVERGED!')
            Thresh = np.sort(Walker_Probs)[-N_Walkers_Sample]
            
            SampleWalkers = Walkers[Walker_Probs >= Thresh] 
            SampleWalker_Probs = Walker_Probs[Walker_Probs >= Thresh] 
        
        else:
            print('Found sufficient number of walkers to be converged! Trimming randomly...')
            mask = (Walker_Probs > Max_Prob-Max_Delta) 

            MaskWalkers = Walkers[mask]
            MaskWalker_Probs = Walker_Probs[mask]
    
            perm = rnd.permutation(len(MaskWalkers))[:N_Walkers_Sample]
            SampleWalkers = MaskWalkers[perm]
            SampleWalker_Probs = MaskWalker_Probs[perm]
    
        MedProb = np.average(SampleWalker_Probs)
        state = SampleWalkers
    
    else:
        Walker_Probs = state.log_prob
        MedProb = np.average(Walker_Probs)
    

    plt.plot(np.arange(len(Likes)), np.exp(Likes),'k')
    plt.plot([0,len(Likes)],[-ref_prob+N_Free/2.,-ref_prob+N_Free/2.],'r--')
    plt.scatter([len(Likes)-1],[-MedProb],c='b',edgecolor='none',marker='o',s=50)
    
    plt.ylim(np.min([np.min(np.exp(Likes)),-ref_prob+N_Free/2., -MedProb])*0.9-1,np.min(np.exp(Likes))*2.+1)
    plt.xlim(0,len(Likes))
    
    
    savename_burnin = '%s/BurninPlot.pdf' % (Dir)
    plt.savefig(savename_burnin,bbox_inches='tight')
    plt.close()
            
    print("Mean acceptance fraction: {0:.3f}".format(
        np.mean(sampler.acceptance_fraction)
    ))
    try:
        print("Mean autocorrelation time: {0:.3f} steps".format(
            np.mean(sampler.get_autocorr_time(tol=0))
        ))
    except Exception as e:
        print(e)
    
    
    ### Actual sampling!
    print('Sampling production run...')
    if N_Walkers != N_Walkers_Sample:
        sampler = final_sampler
    
    sampler.reset()
    state = sampler.run_mcmc(state, N_Sample, progress=True)
    
    
    """ Check output """
    ### First look at output!
    print('Evaluating Output quality...')
    acc_frac = np.mean(sampler.acceptance_fraction)
    try:
        autocorr = np.mean(sampler.get_autocorr_time(tol=0))
        autocorr = N_Sample/10 if autocorr != autocorr else autocorr
    except Exception as e:
        autocorr = N_Sample/10.
        print(e)
        
    print("Mean acceptance fraction: {0:.3f}".format(acc_frac))            
    print("Mean autocorrelation time: {0:.3f} steps".format(autocorr))
        

    print('Getting Output...')
    thinner = max(2,int(autocorr/10.))
    samples = sampler.get_chain(flat=True, thin=thinner)
    likes = sampler.get_log_prob(flat=True, thin=thinner)
    
    ### Mask all those params with terrible probabilities! 
    MedStat = np.max(likes)
    Mask = (likes >= MedStat - 50) ### Everything at least np.exp(-50) times as probable as mode!
    if np.sum(Mask)/len(Mask) < 0.5:
        print('Chain not converged; Fraction within Delta log p < 50:',np.sum(Mask)/len(Mask),'Trying to rerun from final state!')
        sampler.reset()
        state = sampler.run_mcmc(state, N_Sample, progress=True)
        
        acc_frac = np.mean(sampler.acceptance_fraction)
        try:
            autocorr = np.mean(sampler.get_autocorr_time(tol=0))
            autocorr = N_Sample/10 if autocorr != autocorr else autocorr
        except Exception as e:
            autocorr = N_Sample/10.
            print(e)

    
        thinner = max(2,int(autocorr/10.))
        samples = sampler.get_chain(flat=True, thin=thinner)
        likes = sampler.get_log_prob(flat=True, thin=thinner)
        
        ### Mask all those params with terrible probabilities! 
        MedStat = np.max(likes)
        Mask = (likes >= MedStat - 50) ### Everything at least np.exp(-50) times as probable as mode!
        print('Now have:',np.sum(Mask)/len(Mask))
        
    ### Mask out possible unconverged samples
    samples = samples[Mask]
    likes = likes[Mask]
    
    
    """ Post-processing: Create result plots! """    
    ### Quick check of convergence!
    print('Creating Trace Plot...')
    walker_samples = sampler.get_chain(flat=False)
    walker_likes = sampler.get_log_prob(flat=False)
    
    plt.figure(figsize=(10,4*(N_Free+1)))
    for n in range(N_Free):
        plt.subplot(N_Free+1,1,n+1)
        for i in range(5):
            par = walker_samples[:,i,n]
            plt.plot(par,lw=0.5)#,color='k')
            plt.ylabel(param_names[n])
    
    plt.subplot(N_Free+1,1,N_Free+1)### Plot likes too!
    for i in range(5):
        par = walker_likes[:,i]
        plt.plot(par,lw=0.5)#,color='k')
        plt.ylabel(r'log $L$')
    
    
    plt.subplots_adjust(hspace=0.1)
    
    savename_conv = '%s/MCMC_Convergence.pdf' % (Dir)
    plt.savefig(savename_conv,bbox_inches='tight')
    plt.close()                
    
    
    print('Saving Results...')
    ### Try to package into data frame!
    Cat = {param_names[i]:samples[:,i] for i in range(N_Free)}
    Cat['L'] = likes ### Add likelihood
    
    df = pd.DataFrame(Cat, columns=list(param_names)+['L'])
    del(Cat)
    
    savename_chain = '%s/MCMC_SamplerResults' % (Dir)
    pd.to_pickle(df,savename_chain)

    
    
    Labels = param_names ### Nicer names!

    Data = samples # np.transpose([Samples.field(i) for i in Names])
    Prob = likes
    
    Medians = np.median(Data,axis=0)
    ULs = np.percentile(Data,84,axis=0)
    LLs = np.percentile(Data,16,axis=0)
    ULs95 = np.percentile(Data,97.5,axis=0)
    LLs95 = np.percentile(Data,2.5,axis=0)
    
    Errors = [Medians - LLs, ULs - Medians]      
    
    ### Write results
    file = open('%s/Results_%s_MCMC.txt' % (Dir, Srcmodel.split('*')[-1]),'w') ### File name should be dependent of source model
         
    print('MCMC Results')
    print('------------')
    file.write('MCMC Results \n')
    print('------------ \n')
    
    for i in range(N_Free):
        print('log' if logmask[i] else '   ', Labels[i],':',Medians[i],'+',Errors[1][i],'-',Errors[0][i])
        file.write('%s %s: %.6f + %.6f - %.6f \n' % ('log' if logmask[i] else '   ', Labels[i],Medians[i],Errors[1][i],Errors[0][i]))
    
    print('log likelihood',np.median(Prob),'+',np.percentile(Prob,84)-np.median(Prob),'-',-np.percentile(Prob,16)+np.median(Prob))    
    file.write('log likelihood: %.2f + %.2f - %.2f \n' % (np.median(Prob),np.percentile(Prob,84)-np.median(Prob),-np.percentile(Prob,16)+np.median(Prob)))
    file.close()

    """ Finally, get a flux chain & filter models """
    print('Getting Flux and Posterior Predictive Chains...')
    
    Plot.device = '/null'
    Plot.xAxis = "keV"
    Plot.addCommand('rescale x %s %s' % (str(emin),str(emax)))
    Plot.add = True
    Plot.setRebin(5 ,1000, groupNum=1) 

    ### Infer number of additive components!
    BadComp = ['abs','expfac','const']
    CompNames = [AllModels(1,mod).componentNames for mod in Models]
    GoodCompNames = [[j for j in i if not np.sum([b in j for b in BadComp])] for i in CompNames]
    NComp = int(np.sum([len(j) for j in GoodCompNames])) ### Wont work for single component models!
    NSourceComp = len(GoodCompNames[0])
    ### Remove one if only one source!
    NComp = NComp - 1*(NSourceComp==1)
    
    def StuffExtractor(TryPar, mode='flux',emin_flux=0.2, emax_flux=5.0):
        pardicts = []
#        for i, mod in enumerate(Models):
        for n in range(1,N_spec+1):
            for j, mod in enumerate(Models):
                i = j+(n-1)*len(Models)

                Inds = (IndMapper[:,0] == i) ### Whether or not each param corresponds to this model component 
                Nums = IndMapper[:,1][Inds] ### Numbers of parameters to be set
                
                locpar = TryPar[Inds]
                dic = {int(Nums[k]+1):locpar[k] for k in range(len(Nums))}
                
                try:
                    m = AllModels(1,mod)
                except:
                    continue
                
                pardicts += [m, dic]
        AllModels.setPars(*pardicts)
    
        if mode == 'flux':
            AllModels(1,Models[0]).TBabs.nH.values = 0.
            AllModels.calcFlux("%.2f %.2f" % (emin_flux, emax_flux))    
            
            fluxes = np.array(AllData(1).flux) 
            fluxes = fluxes.reshape(-1,6)
#                    for f in fluxes:
#                        if (f > 0).any(): ### should be first one!
#                            subflux = f
            subflux = fluxes[-1]

            flux = subflux[0]            
            return flux
        
        elif mode == 'plot':
            Plot('ldata')
            modvalues = Plot.model(1,1)
            
            return modvalues
        
        elif mode == 'both':
            ### Get spectrum
            AllData.show()
            Plot('ldata')
            modvalues = Plot.model(1,1)
            
            ### Try getting individual components!                    
            AddComponents = []
            if NComp > 1:
                for j in range(1,1+NComp):
                    try:
                        AddComponents.append(Plot.addComp(addCompNum=j, plotGroup=1, plotWindow=1))
                    except Exception as e:
                        break
            modvalues = np.array([modvalues] + AddComponents)

            
            ### Get UNABORBED flux
            if 'tbabs' in Srcmodel:
                AllModels(1,Models[0]).TBabs.nH.values = 0.
            AllModels.calcFlux("%.2f %.2f" % (emin_flux, emax_flux))
            
            fluxes = np.array(AllData(1).flux) ### HOW TO GET FLUX!?!?!
            fluxes = fluxes.reshape(-1,6)

            subflux = fluxes[-1]
            flux = subflux[0]      
            
            return flux, modvalues
        
    ### Flux & Model chain
    N_Sample = 500 # ### How often to sample flux & models!--> 5000 seems like good sample!
    if len(samples) >= N_Sample: ### Long enough to sample randomly
        SampleNumbers = rnd.permutation(len(samples))[:N_Sample]
    else: ### Need to artificially upsample! Should only apply in a few cases!
        NUp = int(N_Sample/len(samples)) + 1
        SampleNumbers = np.array(list(rnd.permutation(len(samples)))*NUp)[:N_Sample]
    
    Fluxes = []
    ModelSpecs = []
    
    def extract(s):
        exp_samples = 10**samples[s]
        exp_samples[(exp_samples!=exp_samples) + (np.isinf(exp_samples))] = 0 ### safeguard against insane values
        locpar = samples[s]*linmask + exp_samples*logmask
        
        f, modval = StuffExtractor(locpar, mode='both',emin_flux=0.2, emax_flux=5.0)
        return f, modval
    
    todo = tqdm(SampleNumbers)   
    
    if False:
#    if parallel: #--> doesnt work!?!
        num_cores = min(max_cores, multiprocessing.cpu_count())
        if __name__ == "__main__":
            Res = Parallel(n_jobs=num_cores)(delayed(extract)(s) for s in todo)
        for f, modval in Res:
            Fluxes.append(f)
            ModelSpecs.append(modval)

    else:
        for s in todo:
            f, modval = extract(s)
            Fluxes.append(f)
            ModelSpecs.append(modval)
        
        
    xdata = Plot.x(1,1)
    xerr = Plot.xErr(1,1)
    ydata = Plot.y(1,1)
    yerr = Plot.yErr(1,1)
    
    Fluxes = np.array(Fluxes)

    ### Rearrange into proper shape!
    ModelSpecs = np.transpose(ModelSpecs,axes=(1,0,2))### Get model type as first axis!  
    if NSourceComp > 1:
        SourceModelSpecs = ModelSpecs[:NSourceComp+1] ### Including source model!
        BackModelSpecs = np.sum(ModelSpecs[NSourceComp+1:],axis=0)
    else: ### Needs to artifically be derived!
        BackModelSpecs = np.sum(ModelSpecs[1:],axis=0)
        SourceModelSpecs = [ModelSpecs[0], ModelSpecs[0] - BackModelSpecs]

    ModelSpecs = np.array([i for i in SourceModelSpecs] + [BackModelSpecs])
    
    ### Plots of the chains
    m_med = np.median(ModelSpecs,axis=1)
    m_lo, m_hi = np.percentile(ModelSpecs,16,axis=1), np.percentile(ModelSpecs,84,axis=1)
    m_lo95, m_hi95 = np.percentile(ModelSpecs,2.5,axis=1), np.percentile(ModelSpecs,97.5,axis=1)
    
    
    plt.figure(figsize=(10,6))
    plt.errorbar(xdata,ydata,yerr=yerr,xerr=xerr,fmt='none',color='k',elinewidth=1,capsize=0,zorder=1)
    
    colors = ['k','r','purple','b','g','c','orange','limegreen','salmon','grey']
    for i in range(len(m_med)):
        plt.plot(xdata,m_med[i],lw=1.5,color=colors[i],zorder=0)
        plt.fill_between(xdata,m_lo[i],m_hi[i],color=colors[i],alpha=0.2,zorder=0,ec='none')
        plt.fill_between(xdata,m_lo95[i],m_hi95[i],color=colors[i],alpha=0.15,zorder=0,ec='none')
        
    
    plt.yscale('log')
    plt.ylim(bottom=np.min(m_med[0])/2.,top=np.max(m_med[0])*1.5)
    
    plt.xscale('log')
    plt.xticks([0.2,0.3,0.5,1.0,2.0,3.0,5.0], labels=[0.2,0.3,0.5,1.0,2.0,3.0,5.0])
    plt.xlim(emin,emax)
    
    savename_pred = '%s/MCMC_PosteriorPrediction.pdf' % (Dir)
    plt.savefig(savename_pred, bbox_inches='tight')
    plt.close()


    ChainARRAY = [Fluxes, ModelSpecs, [xdata,ydata,yerr,xerr], SampleNumbers]
    
    savename_chains = '%s/MCMC_FluxSpecChains' % (Dir)
    with open(savename_chains,'wb') as fp:
        pickle.dump(ChainARRAY,fp)
    
    return df, ChainARRAY


""" Here we call the actual fit! """
Results = BkgSimFits(Dir=Dir,Path=Path,Version=version,specname=specname, ### File location
                     backname=backname, ### suffix of background spectrum 
                     Srcmodel=Srcmodel,Bckgmodel=pback,Bckgmodel2=xrb, ### Models
                     emin=emin,emax=emax, ### Range
                     Err=Err, MCMC=MCMC,### Do error analysis?
                     Pars = Pars, ### Set source spectrum parameters 
                     Lows = Lows, ### Lower limits 
                     Highs = Highs, ### Upper limits
                     Frozen = Frozen, ### Which parameters to freeze
                     BckgPars = BckgPars, ### Parameters of particle background
                     BckgPars2 = xrb_pars, ### Parameters of X-ray background
                     BckgFrozen2 = xrb_Frozen,
                     link_pnorm=True, link_xnorm=True, ### Whether to link global normalizations of background between source & background regions 
                     rebin_plot = rebin_plot, 
                     N_Burn = 2000, N_Split = 1, N_Sample = 2000, N_Walkers_Burnin = 100, N_Walkers_Sample = 100,
                     relscale = 0.1, parallel=True, max_cores=16, 
                     ) 

### If the fit takes a long time, it might make sense to save the results like this:
if not MCMC:
    with open('%s/%s_Results' % (Dir, Save), 'wb') as fp:
        pickle.dump(Results, fp)


""" Retrieve fit results """    
if MCMC:
    Fluxes, ModelSpecs, [xdata,ydata,yErrs,xErrs], SampleNumbers = Results[1]
    m_med = np.median(ModelSpecs,axis=1)
    m_lo, m_hi = np.percentile(ModelSpecs,16,axis=1), np.percentile(ModelSpecs,84,axis=1)
    m_lo95, m_hi95 = np.percentile(ModelSpecs,2.5,axis=1), np.percentile(ModelSpecs,97.5,axis=1)
    
    yratio = (ydata-m_med[0])/yErrs
    yratioErr = np.ones_like(yratio)
    yratio_lo, yratio_hi = (m_lo[0]-m_med[0])/yErrs, (m_hi[0]-m_med[0])/yErrs
    yratio_lo95, yratio_hi95 = (m_lo95[0]-m_med[0])/yErrs, (m_hi95[0]-m_med[0])/yErrs
    modvalues = list(m_med[0])
    
    xmod = list(np.array(xdata) - np.array(xErrs))
    xmod.append(xdata[-1] + xErrs[-1])
    modvalues.append(modvalues[-1])

else:
    Pars = Results[0]
    xdata,xErrs,ydata,yErrs,yratio,yratioErr,modvalues,Bxdata,BxErrs,Bydata,ByErrs,Bmodvalues,Sourcexdata,Sourcemodvalues= Results[1]
    CSTAT = Results[2]
    Errs = Results[3]


""" Start plotting """    
f, (ax0, ax1) = plt.subplots(2,1, gridspec_kw={'height_ratios': [2, 1]},figsize=(12,10)) ### Set up figure 

if not MCMC:
    ### Plot datapoints & model ###
    ax0.errorbar(xdata,ydata,xerr=xErrs,yerr=yErrs,color='k',label='Source data',fmt='none',elinewidth=1,capsize=0,zorder=1)
    ax0.step(xdata,modvalues,where='mid',color='k',label='Total model',linewidth=1)
       
    ### Plot background data
    ax0.errorbar(Bxdata,Bydata,xerr=BxErrs,yerr=ByErrs,color='r',label='Background data',fmt='none',elinewidth=1,capsize=0)
    ### Plot background model
    ax0.step(Bxdata,Bmodvalues,where='mid',color='r',label='Background model',linewidth=1)
    
    ### Plot pure source model
    ax0.plot(Sourcexdata,Sourcemodvalues,color='b',label='Source Model',linewidth=1.5)
    
    ### Plot Residuals on second axis
    ax1.errorbar(xdata,yratio,xerr=xErrs,yerr=yratioErr,color='k',fmt='none',elinewidth=1,capsize=0)

else:
    ### Plot data & ratio
    ax0.errorbar(xdata,ydata,xerr=xErrs,yerr=yErrs,color='k',label='Source Spectrum',fmt='none',elinewidth=1,capsize=0,zorder=1)
    ax1.errorbar(xdata,yratio,xerr=xErrs,yerr=yratioErr,color='k',fmt='none',elinewidth=1,capsize=0,zorder=1)
                
    for i in range(len(m_med)):
        if i == 0:
            ax0.plot(xdata,m_med[i],lw=1.5,color='k',label='Total model',zorder=0)
            ax0.fill_between(xdata,m_lo[i],m_hi[i],color='k',alpha=0.2,zorder=0,ec='none')
            ax0.fill_between(xdata,m_lo95[i],m_hi95[i],color='k',alpha=0.15,zorder=0,ec='none')
        else:
            ax0.plot(xdata,m_med[i],lw=1.5,label='Component %i' % i,zorder=0)
            ax0.fill_between(xdata,m_lo[i],m_hi[i],alpha=0.2,zorder=0,ec='none')
            ax0.fill_between(xdata,m_lo95[i],m_hi95[i],alpha=0.15,zorder=0,ec='none')

    ax1.fill_between(xdata,yratio_lo,yratio_hi,color='k',alpha=0.2,zorder=0,ec='none')
    ax1.fill_between(xdata,yratio_lo95,yratio_hi95,color='k',alpha=0.15,zorder=0,ec='none')

""" Set axis limits & labels """
ymin, ymax = np.min(modvalues)*0.5, 2*max(np.max(modvalues), np.max(ydata))
yratiomin, yratiomax = np.min(yratio) - 1, np.max(yratio) + 1

### Make main plot beautiful ###
ax0.set_xscale('log')
ax0.set_yscale('symlog',linthresh=1E-5)
ax0.set_xticks([0.2,0.5,1.0,2.0,5.0,10.0])
ax0.set_xticklabels([])
ax0.legend(loc=1,fontsize=14,numpoints=1,scatterpoints=1,framealpha=0.)

if grid_plot:
    ax0.grid(True,which='both',linestyle='--',alpha=0.3)
else:
    ax0.grid(False)
ax0.tick_params(reset=False,which='major',axis='x',direction='in',length=15,width=2,colors='k',labelcolor='k')  
ax0.tick_params(which='minor',axis='x',direction='in', length=7.5, width=1.5, colors='k',labelcolor='k')        
ax0.tick_params(reset=False,which='major',axis='y',direction='in',length=10,width=2,colors='k',labelcolor='k')  
ax0.tick_params(which='minor',axis='y',direction='in', length=5, width=1.5, colors='k',labelcolor='k')        

ax0.set_ylabel(r'Count rate $(\mathrm{s}^{-1}\,\mathrm{keV}^{-1})$',fontsize=18)
ax0.set_yticks([1E-3,1E-2,1E-1,1E0,1E1,1E2,1E3,1E4])
ax0.set_yticklabels([r'$10^{%.0f}$' % i for i in np.linspace(-3,4,8)],fontsize=18)
minors = np.ravel(([(np.linspace(2,9,8)*10**i) for i in np.linspace(-3,4,8)]))
ax0.set_yticks(minors, minor=True)
ax0.set_ylim(ymin, ymax)
ax0.set_xlim(eminplot,emaxplot)     


### Residual plot ###
ax1.plot([1E-5,100],[0,0],'k-')
ax1.set_xscale('log')

if grid_plot:
    ax1.grid(True,which='both',linestyle='--',alpha=0.3)
else:
    ax1.grid(False)
ax1.tick_params(reset=False,which='major',axis='x',direction='in',length=15,width=2,colors='k',labelcolor='k')  
ax1.tick_params(which='minor',axis='x',direction='in', length=7.5, width=1.5, colors='k',labelcolor='k')        
ax1.tick_params(reset=False,which='major',axis='y',direction='in',length=10,width=2,colors='k',labelcolor='k',labelsize=18)  
ax1.tick_params(which='minor',axis='y',direction='in', length=5, width=1.5, colors='k',labelcolor='k')        

ax1.set_ylabel(r'$\chi$',fontsize=24)
ax1.set_xlabel(r'$E\,(\rm{keV})$',fontsize=24)
ax1.set_ylim(yratiomin, yratiomax)
ax1.set_xticks([0.2,0.5,1.0,2.0,5.0,10.0])
ax1.set_xticklabels([r'$%.1f$' % i for i in [0.2,0.5,1.0,2.0,5.0,10.0]],fontsize=18)
ax1.set_xlim(eminplot,emaxplot) 


plt.subplots_adjust(hspace=0.0,wspace=0.0) ### No gap between two plots 

### SAVE & DISPLAY ###
if Save == '':
    Savename = '%s/SpectrumSimFit' % Dir
else:
    Savename = '%s/%s' % (Dir, Save)

if MCMC:
    plt.savefig('%s_MCMC.pdf' % Savename,bbox_inches='tight')
else:
    plt.savefig('%s.pdf' % Savename,bbox_inches='tight')

plt.close()



