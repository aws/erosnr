 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 13:59:35 2019

@author: mmayer
"""

from scipy import ndimage
from astropy.io import fits #import packages
import numpy as np
from astropy.wcs import WCS as PyWCS
import os, os.path
import subprocess

def Standard(directory, custominname='', customoutname='', skyfield='', config='', processing='', prefix='', 
             TM=[1, 2, 3, 4, 5, 6, 7],pattern=15, SourceFile='',BackgroundFile=None,separate=True,
             TODO='SPEC EVENTS ARF RMF',
             extent='TOPHAT',extmap='', 
             tstep=0.1,xgrid=1.0,GTI='GTI',
             ebands=[0.2, 0.5, 1.0, 2.0, 4.5, 10.0],eband_min = '',eband_max = '',eband_selected=[0,1,2,3,4], 
             lctype='SRCGTI-',lcpars='1000',
             ):

    """ 
    This function is a Python wrapper around srctool, and allows one to execute the spectra extraction of extended regions in a reproducible manner. 
    It does not really provide much beyond that, so if command-line execution is preferred, that is certainly ok!
    
    The following parameters are important (The name of the files can be specified either in custom way via custominname, customoutname, or via standard names via skyfield, config, processing, prefix):
        - directory: The directory where the event file is located
        - custominname: Name of event file
        - customoutname: Prefix of output name
        - skyfield: In case of standard event file naming (see eSASS cookbook): Specify the skyfield of the event file (e.g. '123123')
        - config: Sometimes needed in event files directly from the archive, to indicate different versions
        - processing: Processing (e.g. '020') of the data
        - prefix: Prefix of event file, indicating which eRASS and which owner (e.g. 'sm04', 'eb01', ...)
        - TM: which TMs to use for spectra (usually [1,2,3,4,6])
        - pattern: Which patterns to include (only 15 trustworthy right now)
        - SourceFile, BackgroundFile: Names of the source and background specification files, can be fits masks, or simple ds9 regions
        - separate: Whether to extract separate RMFs and ARFs for source and background spectra. (Recommended once regions sizes become comparable to instrument FoV)
        - TODO: Which products to extract. SPEC: spectra, ARF: ancillary response file, RMF: response matrix file, EVENTS: Event file containing only events from source/background regions
        - extent: Specifies assumed source morphology. Could be 'TOPHAT' (reasonable default), 'POINT' (usually not the case here), or 'MAP' (see next)
        - extmap: if 'MAP', supply a fits image giving the brightness distribution of the source. Can be useful for extended sources with very irregularly distributed brightness. Effect is usually minor for survey data, but could be important for pointings.
        - tstep: Time steps on which source coordinates are evaluated for ARF calculation. Supplying a small value (< 1s) not necessary for extended sources, and increases compuation time!
        - xgrid: Spatial sampling for calculation of ARF (in units of 8 arcsec, i.e. eROSITA pixel size). I recommend a value > 1/100 of region diameter, as processing time ~ xgrid^-2
        - GTI: Type of good-time-interval to use. I would recommend doing flaregti filtering via the event list directly, so that here a simple "GTI" is sufficient.
        
    The remaining parameters are essentially obsolete, and only are important for light curve generation, which is usually not interesting for extended sources!
    """

    ### ALLOW FOR NO CONFIG TO BE SPECIFIED!
    if config != '':
        config = '_'+config
        
    if custominname:
        infile_clean = "%s/%s" % (directory, custominname) 
    else:
        #    infile_clean="%s/%s_%s_020_CleanEventList%s_c%s.fits" %(directory, prefix, skyfield, config, processing)
        infile_clean="%s/%s_%s_020_EventList%s_c%s.fits" %(directory, prefix, skyfield, config, processing)

    if customoutname=='':
        outfile="%s/%s_%s_" % (directory, prefix, skyfield)
    else:
        outfile="%s/%s" % (directory, customoutname)  
        
    if config:
        suffix_srctool="%s_c%s" % (config,processing)
    else:
        suffix_srctool = ''
    
    # run evtool for energy bands and gti
    if eband_min == '':
        emin_kev=ebands[:-1]
        emax_kev=ebands[1:]
    else:
        emin_kev=eband_min
        emax_kev=eband_max

    emin_kev = [emin_kev[i] for i in eband_selected]
    emax_kev = [emax_kev[i] for i in eband_selected]
    str_emin_kev = [str(i) for i in emin_kev]
    str_emax_kev = [str(i) for i in emax_kev]
    emin_ev=[int(i*1000) for i in emin_kev]
    emax_ev=[int(i*1000) for i in emax_kev]
    eband = [str(i+1) for i in range(len(emin_kev))]    
    subdir="./"
    srctool_dir="srctool_products/"
    TM = [str(i) for i in TM]
    
    
    if pattern ==15:
        PatStr = ''
    elif pattern ==1:
        PatStr = '_Singles'
    elif pattern ==3:
        PatStr = '_Doubles'
    elif pattern ==7:
        PatStr = '_Triples'
    
    if 'fits' in SourceFile:
        cosys = 'fk5'
        cosys2 = 'mask'

        regline = '%s/%s' % (directory,SourceFile)
        with fits.open(regline) as hdul:                
            header = hdul[0].header                
            Image = hdul[0].data
        w = PyWCS(header)
        CoM = ndimage.measurements.center_of_mass(Image)
#            print(CoM)
        racen,deccen = w.all_pix2world(CoM[1],CoM[0],0)
#            print(racen,deccen)
        coords = '%.6f, %.6f' % (racen,deccen)      
        
        ### FINALLY, ADJUST THE NAMES OF THE RESULTING FILE!
        if '/' in SourceFile:
            SourceFile = SourceFile.split('/')[-1] ### Last part!
        
        if '.fits.gz' in SourceFile:
            SourceFile = SourceFile.replace('.fits.gz','')
        else:
            SourceFile = SourceFile.replace('.fits','')
            
            
        
    else:
            
        file = open('%s/%s.reg' % (directory,SourceFile))
        lines = []
            
        for line in file:
            lines.append(line)
        for i in range(len(lines)):
            if '(' in lines[i]:
                coords = lines[i].split('(')[1].split(',')[:2]
                coords= coords[0] + ','+ coords[1]
                cosys = lines[i-1].split('\n')[0]
                regline= lines[i].split('\n')[0]
                break ## FOR NOW ONLY SINGLE REGIONS!
        cosys2 = cosys+';'
        
        
    if BackgroundFile != None:
        if 'fits' in BackgroundFile:
            bcosys='fk5'
            bregline = '%s/%s' % (directory,BackgroundFile)
            bstring = 'mask %s' % (bregline)
        
            with fits.open(bregline) as hdul:                
                bheader = hdul[0].header                
                bImage = hdul[0].data
            w = PyWCS(bheader)
            bCoM = ndimage.measurements.center_of_mass(bImage)
            bracen,bdeccen = w.all_pix2world(bCoM[1],bCoM[0],0)
            bcoords = '%.6f, %.6f' % (bracen,bdeccen)      

            BackgroundString = BackgroundFile.replace('.fits','')
        
        else:
            BckgReg = '%s/%s.reg' % (directory,BackgroundFile)
            file = open(BckgReg)
            lines = []
            for line in file:
                lines.append(line)
            for i in range(len(lines)):
                if '(' in lines[i]:
                    bcoords = lines[i].split('(')[1].split(',')[:2]
                    bcoords= bcoords[0] + ','+ bcoords[1]
                    
                    bcosys = lines[i-1].split('\n')[0]
                    bregline= lines[i].split('\n')[0]
                    bstring = '%s; %s' % (bcosys, bregline)
                    break ## FOR NOW ONLY SINGLE REGIONS!
                    
            BackgroundString = BackgroundFile
        
    else:
        bstring = 'NONE'
            
    if(os.path.isdir(os.path.join(subdir, srctool_dir))==False):
        os.mkdir(os.path.join(subdir, srctool_dir))  


    if not separate: ### CREATE SOURCE AND BACKGROUND FILE SIMULTANEOUSLY
        cmd=['srctool',
         'eventfiles=%s' %(infile_clean),
         'prefix=%s' %(os.path.join(subdir,srctool_dir,outfile)),
         'suffix=%s_%s%s' %(suffix_srctool,SourceFile,PatStr),
         'pat_sel=%i' % (pattern),
         'srccoord=%s; %s' % (cosys, coords),
         'srcreg=%s %s' % (cosys2, regline),
         'backreg=%s' % bstring,
         'clobber=yes',
         'todo=%s' % TODO,
         'insts=%s' %((' ').join(TM)),
         'lctype=%s' % lctype,
         'lcpars=%s' % lcpars,
         'lcemin=%s' %((' ').join(str_emin_kev)),
         'lcemax=%s' %((' ').join(str_emax_kev)),
         'lcgamma=1.7',
         'tstep=%s' % str(tstep),
         'xgrid=%s' % str(xgrid),
         'gtitype=%s' % GTI
         ]
        
        if extent == 'TOPHAT':
            cmd.append('exttype=TOPHAT')
            cmd.append('extpars=100000')
            cmd.append('psftype=NONE')
        
        elif extent == 'MAP': 
            cmd.append('exttype=MAP')
            cmd.append('extmap=%s/%s' % (directory, extmap))
            cmd.append('psftype=NONE')
        
        else:
            cmd.append('psftype=2D_PSF')
            cmd.append('exttype=POINT')
    
        print(cmd)
        subprocess.check_call(cmd)
    
    
    

    else: ### CREATE THEM SEPARATELY 
        ### SOURCE 
        cmd=['srctool',
         'eventfiles=%s' %(infile_clean),
         'prefix=%s' %(os.path.join(subdir,srctool_dir,outfile)),
         'suffix=%s_%s%s' %(suffix_srctool,SourceFile,PatStr),
         'pat_sel=%i' % (pattern),
         'srccoord=%s; %s' % (cosys, coords),
         'srcreg=%s %s' % (cosys2, regline),
         'backreg=NONE',
         'clobber=yes',
         'todo=%s' % TODO,
         'insts=%s' %((' ').join(TM)),
         'lctype=%s' % lctype,
         'lcpars=%s' % lcpars,
         'lcemin=%s' %((' ').join(str_emin_kev)),
         'lcemax=%s' %((' ').join(str_emax_kev)),
         'lcgamma=1.7',
         'tstep=%s' % str(tstep),
         'xgrid=%s' % str(xgrid),
         'gtitype=%s' % GTI
         ]
        
        if extent == 'TOPHAT':
            cmd.append('exttype=TOPHAT')
            cmd.append('extpars=100000')
            cmd.append('psftype=NONE')### FOR MODERATELY EXTENDED (NO MAP, ONLY SMALL ELLIPSE!)
        
        elif extent == 'MAP': ### TRY THIS!
            cmd.append('exttype=MAP')
            cmd.append('extmap=%s/%s' % (directory, extmap))
            cmd.append('psftype=NONE')### FOR MODERATELY EXTENDED (NO MAP, ONLY SMALL ELLIPSE!)
        
        else:
            cmd.append('psftype=2D_PSF')
            cmd.append('exttype=POINT')
    
        print(cmd)
        subprocess.check_call(cmd)

        if BackgroundFile != None:
            ### BACKGROUND 
            cmd=['srctool',
             'eventfiles=%s' %(infile_clean),
             'prefix=%s' %(os.path.join(subdir,srctool_dir,outfile)),
             'suffix=%s_%s%s' %(suffix_srctool,BackgroundString,PatStr),
             'pat_sel=%i' % (pattern),
             'srccoord=%s; %s' % (bcosys, bcoords),
             'srcreg=%s' % (bstring),
             'backreg=NONE',
             'clobber=yes',
             'todo=%s' % TODO,
             'insts=%s' %((' ').join(TM)),
             'lctype=%s' % lctype,
             'lcpars=%s' % lcpars,
             'lcemin=%s' %((' ').join(str_emin_kev)),
             'lcemax=%s' %((' ').join(str_emax_kev)),
             'lcgamma=1.7',
             'tstep=%s' % str(tstep),
             'xgrid=%s' % str(xgrid),
             'gtitype=%s' % GTI
             ]
            
            if extent == 'TOPHAT':
                cmd.append('exttype=TOPHAT')
                cmd.append('extpars=100000')
                cmd.append('psftype=NONE')
            
            elif extent == 'MAP': ### TRY THIS!
                cmd.append('exttype=MAP')
                cmd.append('extmap=%s/%s' % (directory, extmap))
                cmd.append('psftype=NONE')### FOR MODERATELY EXTENDED (NO MAP, ONLY SMALL ELLIPSE!)
            
            else:
                cmd.append('psftype=2D_PSF')
                cmd.append('exttype=POINT')
        
            print(cmd)
            subprocess.check_call(cmd)
            
    return


""" Example call with custom file names """
Standard(directory="Vela", ### Directory of file
        custominname='Vela_Events.fits', customoutname='Test', ### Name of input event file and output prefix
        TM=[1, 2, 3, 4, 6], ### Using only TM8 
        extent = 'MAP', ### Specificying source extent via map
        extmap = 'sm04_merged_020_ExtMap_c020.fits', ### Map of source brightness 
        SourceFile='TestPolygon_mask.fits', ### Region of source spectrum, specified as fits mask
        BackgroundFile = 'Back_mask.fits', ### Background region, specified as fits mask
        separate=True, ### Get source & background spectra with separate ARFs
        TODO='SPEC ARF RMF',
        tstep=1.0, ### Timesteps of 1s for evaluating eROSITA pointing
        xgrid=30., ### Grid of 30 pix = 240" for computing ARF
        )  


""" Example call with eSASS filenaming --> file path eRASS/RawEvents/211105/EXP/sm04_211105_020_EventList_002_c020.fits """
Standard(directory="eRASS/RawEvents/211105/EXP", ### Directory of file
        config='002', ### Config version 
        processing="020", ### c020 processing
        skyfield="211105", ### skytile number
        prefix='sm04', ### eRASS_4 data from MPE hemisphere
        TM=[1, 2, 3, 4, 6], ### Only TM8 
        extent = 'TOPHAT', ### Assume flat angular extent of source
        SourceFile='Src', ### Region of source spectrum, specified as ds9 region file
        BackgroundFile = 'Back', ### Region of background spectrum, specified as ds9 region file
        TODO='SPEC ARF RMF', 
        tstep=1.0,
        xgrid=5.)  


