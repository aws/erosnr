As requested at the erosnr meeting just now here are some instructions for
using the erosnr git it from command line (my preference)

if necessary install git (e.g. under linux)
or on MPE MPCDF computers do :
module load git

git clone https://gitlab.mpcdf.mpg.de/aws/erosnr  my_eronsrdirectory
cd my_erosnrdirectory

git help    (explain all the commands)
git status  (shows the files)
git pull    (gets latest repository)
git add mynewfile     (for a new file)
git commit  -m " new file has been added and -m means this is the message to go with it"
git push   (sends file to repository)

echo "some more stuff" >>mynewfile
git commit -a -m "mynewfile was changed, -a means consider all files which changed"
git push
git pull (should say 'up to date' or get other peoples changes)

other usual commands like
git mkdir mystuff
git ls

in case you rm a file (not "git rm myfile" which removes it from the repository,  but just "rm myfile" in your copy) and you want to get it back from the repository, do

git checkout myfile

though checkout is usually for switching branches


That is about all I ever use in git, it can do much more if needed.

It is visible only to members, who get an invitation from me.
Let me know if you want one.

If you have an MPCDF account you can use your usual id and pw.
External people have to be invited explicitly to use MPCDF gitlab to be able to use it.

Andy
