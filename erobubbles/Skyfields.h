class Skyfields
{
 public:

  valarray<double> ra_min, ra_max, ra_cen;
  valarray<double>dec_min,dec_max,dec_cen;
  valarray<int> smapnr;

  void read_SKYMAPS();
  void get_skyfield(int smapnr_,
                    double  &ra_min_, double  &ra_max_, double  &ra_cen_,
		    double &dec_min_, double &dec_max_, double &dec_cen_,
                    int    &status);

};
