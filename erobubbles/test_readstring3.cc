

using namespace std;
#include<iostream>
#include<cstdio>   
#include<cstring>          
#include<valarray>
#include <sstream>
#include <string>

//based partly on ~/propagate/c/gitlab/galplot/source/Galdef.cc

/////////////////////////////////////////////////////////
int read_event_filename_list(string event_filename_list_infile, valarray<string> &event_filename_list,int nline,int debug)
{
  cout<<"read_event_file_list"<<endl;
  cout<<"event_filename_list_infile="<<event_filename_list_infile<<endl;
  cout<<"nline="<<nline<<endl;
  cout<<"debug="<<debug<<endl; 
 
  char  filename[200];
  strcpy(filename,event_filename_list_infile.c_str());
  
  FILE *ft;
  ft=fopen(filename,"r");
  if(ft==NULL) {
      cout<<"no input event filename list called "<<filename<<endl; return -1;
  }
 


//int Galdef::read_galdef_parameter(char *filename, char *parstring, char *value)


   char input[37];
   int stat=0;

// ls -1 e*fits >> input_testfile
// this give 36 chars due to \0 or newline  but want 35 hence need to truncate

valarray<string> instring;

instring.resize(nline);

for(int iline=0;iline<nline;iline++)
{

    //https://www.cplusplus.com/reference/cstdio/fgets/
    
 fgets(input,800,ft);
    

 // cout<<"printf:"<<endl;
 // printf("%s",input);       // string is \0 terminated
 
 if(debug==1)cout<<"strlen(input)="<<strlen(input)<<endl;

instring[iline]=input;

 if(debug==1)cout<<"instring= "<<instring[iline]<<endl;

cout<<"iline="<<iline<<" instring="<<instring[iline]<<endl;

 if(debug==1)cout<<"instring length before resize="<<instring[iline].length()<<endl;

 instring[iline].resize(35);
 if(debug==1)cout<<"instring length after resize="<<instring[iline].length()<<endl;

/*
 cout<<"testing with explicit event file"<<endl;
strcpy(input,"eb01_108051_020_EventList_c946.fits");
cout<<"strcmp "<<strcmp(input,"eb01_108051_020_EventList_c946.fits")<<endl;
*/

  } // iline



 cout<<"---------testing open on instring"<<endl;

for(int iline=0;iline<nline;iline++)
  {
 
    if(debug==1)
    {
     cout<<"iline="<<iline<<" "<<instring[iline]<<endl;
     cout<<"length="<<instring[iline].length()<<endl;
     cout<<"instring="<<instring[iline]<<endl;
     cout<< "c_str= " <<instring[iline].c_str()  <<endl;
     }

 FILE *eventft;
       eventft=fopen(instring[iline].c_str(),"r");

       if(eventft!=NULL) cout<<"opened file "<<instring[iline]<< "c_str= " <<instring[iline].c_str()<<" eventft="<<eventft  <<endl;

  if(eventft==NULL) cout<<"no file called "<<instring[iline]<< "c_str= " <<instring[iline].c_str()  <<endl;
  
  fclose(eventft);

}
   
   fclose(ft); 

   event_filename_list=instring;

   cout<<"number of event files read="<<event_filename_list.size()<<endl;

   return stat;
}

//////////////////////////////////////////
int main()
{
  cout<<"test_readstring.cc"<<endl;

  string event_filename_list_infile;
  event_filename_list_infile="input_testfile4";

  valarray<string> event_filename_list;
  int nline;
  nline=63;
  nline=189;

  int debug=0;

  read_event_filename_list(event_filename_list_infile,event_filename_list,nline,debug);

  cout<<"event_filename_list.size()= "<<event_filename_list.size()<<endl;
  
  for(int ifile=0;ifile<event_filename_list.size();ifile++)
  {
    cout<<"returned from read_event_filename_list: ifile="<<ifile<< " "<<event_filename_list[ifile]<<endl;

    
 FILE *eventft;
       eventft=fopen(event_filename_list[ifile].c_str(),"r");
	
       cout<<"eventft=" <<eventft<<endl;

  if(eventft!=NULL) 
cout<<"opened returned file called "<<event_filename_list[ifile]<< "  c_str= " <<event_filename_list[ifile].c_str()  <<endl;

 if(eventft==NULL) 
cout<<"no returned file called "<<event_filename_list[ifile]<< "  c_str= " <<event_filename_list[ifile].c_str()  <<endl;

  
  fclose(eventft);  
    
  }
  

  return 0;
}
