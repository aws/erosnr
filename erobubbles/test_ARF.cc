

using namespace std;
#include<iostream>
#include<cstdio>   
#include<cstring>          
#include<valarray>
#include"fitsio.h"

#include <sstream>
#include <string>

#include"ARF.h"

////////////////////////////////////////////////////////
    
                              

int main()
{ 
  cout<<"ARF.cc"<<endl;

  ARF arf;
  int verbose=1;
  arf.read_ARF(verbose);
  arf.print_ARF(verbose);

  double energy=1.1;
  int status;
  double specresp;
  // specresp=arf.get_ARF(energy,verbose,status);
  cout<<"energy="<<energy<<" specresp="<<specresp<<endl;


  double energy_min=0.2;
  double energy_max=0.7;
  double spectral_index=2;
  specresp=arf.get_ARF(energy_min,energy_max,spectral_index ,verbose,status);
  cout<<"energy_min="<<energy_min<<" energy_max="<<energy_max<<" spectral_index="<<spectral_index<<" specresp="<<specresp<<" status="<< status<<endl;

   energy_min=0.7;
   energy_max=1.2;
  
  specresp=arf.get_ARF(energy_min,energy_max,spectral_index ,verbose,status);
  cout<<"energy_min="<<energy_min<<" energy_max="<<energy_max<<" spectral_index="<<spectral_index<<" specresp="<<specresp<<" status="<< status<<endl;



  
  cout<<"===   ARF.cc complete"<<endl;

  return 0;
}
