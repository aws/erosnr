
using namespace std;
#include<cstdio>    
#include<string>    
#include<fstream>   
#include<iostream>

#include"EroBubbles.h"

int main()
{
  cout<<"bubbles_model"<<endl;
  EroBubbles erobubbles;

  double r_minN_;
  double r_maxN_;
  double r_minS_;
  double r_maxS_;

  double xcN_, ycN_, zcN_;
  double xcS_, ycS_, zcS_;

  string RLH_;

  r_minN_=5.5;
  r_maxN_=6.0;


  r_minS_=5.0;
  r_maxS_=5.5;


  double ds_;
  double s_max_;
  double R0_;

  ds_   = 0.02;
  s_max_= 20;
  R0_   = 8.;

  int model;
  model=3;

  int order;
  string outfile;
  double E;
  int healpix_debug;

  order=7;
  outfile="bubbles.fits";
  E = 3;
  healpix_debug=0;

  ////////////////////////////////////

  model=9;

  if(model==1)
  {
  // symmetric about plane
   RLH_="R";
   r_minN_=5.5;
   r_maxN_=6.0;
   r_minS_=5.0;
   r_maxS_=5.5;
   xcN_ = +1; ycN_ =+0; zcN_ =+4;
   xcS_ = +1; ycS_ =+0; zcS_ =-4;
   outfile="bubbles_model_1.fits";
  }

  if(model==2)
  {
 // asymmetric about plane  RHS
   RLH_="R";
   r_minN_=5.5;
   r_maxN_=6.0;
   r_minS_=5.0;
   r_maxS_=5.5;
   xcN_ = -1; ycN_ =+1; zcN_ =+6.5;
   xcS_ = +0; ycS_ =-2; zcS_ =-6.5;
   outfile="bubbles_model_2.fits";
  }

  if(model==3)
  {
 // asymmetric about plane  RHS
   RLH_="R";
   r_minN_=5.5;
   r_maxN_=6.0;
   r_minS_=5.0;
   r_maxS_=5.5;
   xcN_ = -1; ycN_ =+1; zcN_ =+6.5;//model 2 for reference
   xcN_ = -1; ycN_ =+1; zcN_ =+8.5;
   xcS_ = +0; ycS_ =-2; zcS_ =-6.5;//model 2 for reference
   outfile="bubbles_model_3.fits";
  }


  if(model==4)
  {
 // asymmetric about plane  RHS
   RLH_="R";
   r_minN_=5.5;
   r_maxN_=6.0;
   r_minS_=5.0;
   r_maxS_=5.5;
   xcN_ = -1; ycN_ =+1; zcN_ =+6.5;//model 2 for reference
   xcN_ = -1; ycN_ =+3; zcN_ =+8.5;
   xcS_ = +0; ycS_ =-2; zcS_ =-6.5;//model 2 for reference
   outfile="bubbles_model_4.fits";
  }

  if(model==5)
  {
 // asymmetric about plane  RHS
   RLH_="R";
   r_minN_=5.5; //model 2 for reference
   r_minN_=6.5;
   r_maxN_=6.0; //model 2 for reference
   r_maxN_=7.0;
   r_minS_=5.0; //model 2 for reference
   r_minS_=6.0;
   r_maxS_=5.5; //model 2 for reference
   r_maxS_=6.5;
   xcN_ = -1; ycN_ =+1; zcN_ =+6.5;//model 2 for reference
   xcN_ = -1; ycN_ =+3; zcN_ =+8.5;
   xcS_ = +0; ycS_ =-2; zcS_ =-6.5;//model 2 for reference
   xcS_ = +0; ycS_ =-0; zcS_ =-6.5;
   outfile="bubbles_model_5.fits";
  }

 if(model==6)
  {
 // symmetric about GC  RHS
   RLH_="R";
   r_minN_=6.5;
   r_maxN_=7.0;
   r_minS_=6.5;
   r_maxS_=7.0;
   xcN_ = +1; ycN_ =+3; zcN_ =+8.5;
   xcS_ = -1; ycS_ =-3; zcS_ =-8.5;
   outfile="bubbles_model_6.fits";
  }


  if(model==7)
  {
 // asymmetric about plane  RHS, as 5 but  1 kpc thick shell
   RLH_="R";
   r_minN_=6.0;
   r_maxN_=7.0;
   r_minS_=5.5;
   r_maxS_=6.5;
   xcN_ = -1; ycN_ =+3; zcN_ =+8.5;
   xcS_ = +0; ycS_ =-0; zcS_ =-6.5;
   outfile="bubbles_model_7.fits";
  }


 if(model==8)
  {
 // asymmetric about plane  RHS, as 5 but  1 kpc thick shell
   RLH_="R";
   r_minN_=5.0;
   r_maxN_=7.0;
   r_minS_=4.5;
   r_maxS_=6.5;
   xcN_ = -1; ycN_ =+3; zcN_ =+8.5;
   xcS_ = +0; ycS_ =-0; zcS_ =-6.5;
   outfile="bubbles_model_8.fits";
  }

 if(model==9)
  {
 // asymmetric about plane  RHS, as 5 but  1 kpc thick shell          apec emissivities
   RLH_="R";
   r_minN_=5.0;
   r_maxN_=7.0;
   r_minS_=4.5;
   r_maxS_=6.5;
   xcN_ = -1; ycN_ =+3; zcN_ =+8.5;
   xcS_ = +0; ycS_ =-0; zcS_ =-6.5;
   outfile="bubbles_model_9.fits";
   order=8;
  }



 
  erobubbles.init( r_minN_, r_maxN_,
                   r_minS_, r_maxS_,
                   xcN_,  ycN_,  zcN_,
		   xcS_,  ycS_,  zcS_,
                   RLH_,
		   ds_, s_max_, R0_);


 

  erobubbles.healpix_skymap(order, outfile, E, healpix_debug);

  return 0;

  ////////////////////////////////////////////////////////////////////////

  double l,b;
  double l_min, l_max, dl;
  double b_min, b_max, db;

  l_min=0;
  l_max=360;
  dl=10;

  b_min=-90;
  b_max=+90;
  db   =10;


  E=3;
  RLH_="R";

 
  for(l=l_min;l<=l_max;l+=dl)
  for(b=b_min;b<=b_max;b+=db)
    cout<<"l="<<l<<" b= "<<b
        <<" intensity result1="<<erobubbles.intensity (l,b,E)<<endl;

 // asymmetric about plane
  xcN_ = +1; ycN_ =+2; zcN_ =+3;
  xcS_ = -1; ycS_ =-2; zcS_ =-4;

  erobubbles.init( r_minN_, r_maxN_,
                   r_minS_, r_maxS_,
                   xcN_,  ycN_,  zcN_,
		   xcS_,  ycS_,  zcS_,
                   RLH_,
                   ds_,   s_max_, R0_);



 
  for(l=l_min;l<=l_max;l+=dl)
  for(b=b_min;b<=b_max;b+=db)
    cout<<"l="<<l<<" b= "<<b
        <<" intensity result2="<<erobubbles.intensity (l,b,E)<<endl;


  return 0;
}
