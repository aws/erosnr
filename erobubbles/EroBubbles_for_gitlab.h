class EroBubbles
{
 public:

  double xcN, ycN, zcN;
  double xcS, ycS, zcS;

  string RLH;
  
  double r_minN, r_maxN;
  double r_minS, r_maxS;

  double ds, s_max;
  double R0;

  int init(double r_minN_, double r_maxN_,
           double r_minS_, double r_maxS_,
           double xcN_, double ycN_, double zcN_,
           double xcS_, double ycS_, double zcS_,
           string RLH_,
           double ds_,   double s_max_,
           double R0_);

  double emissivity(double x, double y, double z,  double E );
  double intensity (double l, double b,            double E );
  int    healpix_skymap(int order, string outfile, double E,int healpix_debug);
  double thermal_emission       ( double em, double T, double E);
  double thermal_emissivity( double density, double T, double Emin, double Emax,int debug);
};
