

using namespace std;
#include<iostream>
#include<cstdio>             
#include<valarray>

#include "healpix_map.h"
#include "healpix_map_fitsio.h"

#include "alm.h"
#include "alm_fitsio.h"
#include "alm_powspec_tools.h"
#include "alm_healpix_tools.h"
#include "powspec.h"

#include "xcomplex.h"
#include "arr.h"
#include "fitshandle.h"

#include "pointing.h"
 
#include "trafos.h"
// #include "cxxsupport.h"  // trafo.h  need in compile path

///////////////////////////////////////////////////////////////////////////////////
// used healpix_processing_cleanupmore.cc
// used healpix_maps_ratios.cc
//////////////////////////////////////////////////////////////////////////////////

double thermal_emission( double em, double T, double E)
{
// thermal formula:
//https://wiki.mpe.mpg.de/eRosita/erosnr_project_Fermi_bubbles?action=AttachFile&do=view&target=erosita_garching_2020_FermiBubbles.pdf

// Longair eq 6.47


 
 double  g=1.2;              // Gaunt factor
 double  k=1.38e-16  ;       // erg/K
 double  keV_to_erg = 1.602e-9;
 //                                                            
 double thermal_emission_= .004 * em * pow(T,-0.5) * g * exp(-(E*keV_to_erg)/(k*T)) ;// cf pdf above. erg cm-2 sr-1 s-1 keV-1    
 
 cout<<"thermal_emission_="<<thermal_emission_<<" erg cm-2 sr-1 s-1 keV-1"<<endl;
 return thermal_emission_;
}
/////////////////////////////////////////////////////////////////////////////

int pixel_select(double l, double b, double l_centre, double b_centre, double radius_min, double radius_max,int verbose)
{
  //cout<<"_pixel_select:"<<endl;
 
  double pi=acos(-1.);
  double dtr=acos(-1.)/180.; // degrees to radians
  double rtd=180./acos(-1.); // radians to degrees

  double theta       = (90.-b)           *dtr;
  double phi         =      l            *dtr;
  double theta_centre= (90.-theta_centre)*dtr;
  double phi_centre  =          l_centre *dtr;
   

  double angle;
  angle=acos (cos(theta)*cos(theta_centre) + sin(theta)*sin(theta_centre) * cos (phi-phi_centre)) * rtd;
  int pixel_select_=0;
  if(angle>=radius_min && angle<=radius_max) pixel_select_=1;
  
  if(verbose==1) cout<<" pixel_select: l="<<l<<" b="<<b<<" l_centre="<<l_centre<<" b_centre="<<b_centre<<" radius_min="<<radius_min<<" radius_max="<<radius_max<<" angle="<<angle<<" pixel_select_="<<pixel_select_<<endl;	     
  
  return pixel_select_;
}
//////////////////////////////////////////////////////////////////////////////////////////////             
int healpix_skymap()
{

  cout<<endl<< "=== healpix_skymap"<<endl<<endl;

  int debug=0;
    
  PDT datatype = PLANCK_FLOAT64; // HealPix defines it this way
      datatype = PLANCK_FLOAT32;
  
 
  arr<double>data; // for HealPix array class
  
  int hdu;
  
  int ncolnum,colnum;
  int order;
  string outfile;


  //==================== 

  outfile   ="healpix_test_out.fits"          ;   

  order=6; 
  ncolnum=1;

  cout<<"test writing healpix skymap to "<<outfile<<endl;


  

  fitshandle out;  
  out.create("!"+outfile); // ! to overwrite

  arr<string> colname;
  colname.alloc(ncolnum);

  for(int j=0;j<ncolnum;j++)   colname[j]="bubbles";

  Healpix_Map<double> map_RING_out(order,RING);
  prepare_Healpix_fitsmap(out,map_RING_out, datatype, colname);
  
  out.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
     
  colnum=1;
 
  cout<< "map_RING_out: ";
  cout<<" Npix  = "<<map_RING_out.Npix(); 
  cout<<" Nside = "<<map_RING_out.Nside();
  cout<<" Order = "<<map_RING_out.Order();
  cout<<" Scheme= "<<map_RING_out.Scheme()<<endl; // 0 = RING, 1 = NESTED

  data.alloc(map_RING_out.Npix());
  
  pointing pointing_;
  
  double l,b;
  double ra,dec;
  double rtd=180./acos(-1.); // radians to degrees 180/pi

  for (int ipix=0; ipix<map_RING_out.Npix(); ipix++)
  {
   pointing_ = map_RING_out.pix2ang(ipix);

   l =     pointing_.phi  *rtd;
   b = 90.-pointing_.theta*rtd;

   data[ipix]= l * b ;

   if(0)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" l="<<l<<" b="<<b<<" data[ipix]= "<<data[ipix]<<endl;
  }

  if(0)
  out.write_column(colnum,data); // fitshandle.h


  out.close();                                                         

  

  //////////////////////////////////////////////////////////////////////////////////


  /////////// DAtool erass1 healpix
  cout<<endl<<"===============DAtool erass1 healpix"<<endl;

  int Nside_factor            =  16;//8;//16;//64; 
  int write_maps              =  1;
  int fastcheck               =  1;  //1=quick low-order Alm

  cout<<"Nside_factor="<<Nside_factor<<endl;
  cout<<"write_maps="<<write_maps<<endl;
  cout<<"fastcheck="<<fastcheck<<endl;

  /////////////////////////////////////////////
  // read
  string infile;
  
  string infile_raw,infile_exp,infile_exp2;

  int band;
  band=21; // kept for definition later
  
  {
  
  
 
  
    
  infile_raw     =    "intensity_from_list_healpix_estart_0.2_eend_8.0_de_0.5_order_8.fits";
  infile_raw     =    "intensity_from_list_healpix_estart_0.2_eend_8.0_de_0.5_order_8.fits_energies1-4";
 
 
 
  Healpix_Map<double>inmap_1,inmap_2,inmap_3,inmap_4;
  Healpix_Map<double>inmap_5,inmap_6,inmap_7,inmap_8;

  colnum=1;
  hdu=2;// HDU 2 (1=primary)  

   cout<<endl;
   
   

   cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_1     ,colnum,hdu);// HDU 2 (1=primary)

 

   
   cout<<" Npix  = "<<inmap_1     .Npix(); 
   cout<<" Nside = "<<inmap_1     .Nside();
   cout<<" Order = "<<inmap_1     .Order();
   cout<<" Scheme= "<<inmap_1     .Scheme()<<endl; // 0 = RING, 1 = NESTED


   //for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_1= "<<inmap_1[ipix]<<endl;

   colnum=2;
   cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_2     ,colnum,hdu);// HDU 2 (1=primary)  

   //  for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_2= "<<inmap_2[ipix]<<endl;

   cout<<" Npix  = "<<inmap_2     .Npix(); 
   cout<<" Nside = "<<inmap_2     .Nside();
   cout<<" Order = "<<inmap_2     .Order();
   cout<<" Scheme= "<<inmap_2     .Scheme()<<endl; // 0 = RING, 1 = NESTED

   colnum=3;
     cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_3     ,colnum,hdu);// HDU 2 (1=primary)  
   //   for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_3= "<<inmap_3[ipix]<<endl;
 

   cout<<" Npix  = "<<inmap_3     .Npix(); 
   cout<<" Nside = "<<inmap_3     .Nside();
   cout<<" Order = "<<inmap_3     .Order();
   cout<<" Scheme= "<<inmap_3     .Scheme()<<endl; // 0 = RING, 1 = NESTED


   colnum=4;
   cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_4     ,colnum,hdu);// HDU 2 (1=primary)  
   //   for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_4= "<<inmap_4[ipix]<<endl;
 

   cout<<" Npix  = "<<inmap_4     .Npix(); 
   cout<<" Nside = "<<inmap_4     .Nside();
   cout<<" Order = "<<inmap_4     .Order();
   cout<<" Scheme= "<<inmap_4     .Scheme()<<endl; // 0 = RING, 1 = NESTED


   // energies 5-8
   
    infile_raw     =    "intensity_from_list_healpix_estart_0.2_eend_8.0_de_0.5_order_8.fits_energies5-8";
    
     colnum=1;
   hdu=2;// HDU 2 (1=primary)  

   cout<<endl;
   
   

   cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_5     ,colnum,hdu);// HDU 2 (1=primary)

 

   
   cout<<" Npix  = "<<inmap_5     .Npix(); 
   cout<<" Nside = "<<inmap_5     .Nside();
   cout<<" Order = "<<inmap_5     .Order();
   cout<<" Scheme= "<<inmap_5     .Scheme()<<endl; // 0 = RING, 1 = NESTED


   //for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_1= "<<inmap_1[ipix]<<endl;

   colnum=2;
   cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_6     ,colnum,hdu);// HDU 2 (1=primary)  

   //  for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_2= "<<inmap_2[ipix]<<endl;

   cout<<" Npix  = "<<inmap_6     .Npix(); 
   cout<<" Nside = "<<inmap_6     .Nside();
   cout<<" Order = "<<inmap_6     .Order();
   cout<<" Scheme= "<<inmap_6     .Scheme()<<endl; // 0 = RING, 1 = NESTED

   colnum=3;
     cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_7     ,colnum,hdu);// HDU 2 (1=primary)  
   //   for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_3= "<<inmap_3[ipix]<<endl;
 

   cout<<" Npix  = "<<inmap_7     .Npix(); 
   cout<<" Nside = "<<inmap_7     .Nside();
   cout<<" Order = "<<inmap_7     .Order();
   cout<<" Scheme= "<<inmap_7     .Scheme()<<endl; // 0 = RING, 1 = NESTED


   colnum=4;
   cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_8     ,colnum,hdu);// HDU 2 (1=primary)  
   //   for (int ipix=0; ipix<inmap_1.Npix(); ipix++) cout<<"ipix="<<ipix <<" inmap_4= "<<inmap_4[ipix]<<endl;
 

   cout<<" Npix  = "<<inmap_8     .Npix(); 
   cout<<" Nside = "<<inmap_8     .Nside();
   cout<<" Order = "<<inmap_8     .Order();
   cout<<" Scheme= "<<inmap_8     .Scheme()<<endl; // 0 = RING, 1 = NESTED

   /////////////////////////////////////////////////////////////////////////////////
 


   double pi=acos(-1.);
   double dtr=acos(-1.)/180.; // degrees to radians
   double rtd=180./acos(-1.); // radians to degrees
   double solid_angle_pixel=4*pi/inmap_1.Npix();
   double sa_arcminsq=pow(1/(rtd*60.),2); // arcminsq in sr
   double sa_arcdegsq=pow(1/(rtd    ),2); // arcdeg in sr
   double pixel_arcminsq=solid_angle_pixel/sa_arcminsq;
   double pixel_arcmin_side=sqrt(pixel_arcminsq);
   double solid_angle_datool=pow(4./(rtd*60.*60.) ,2); // 4 arcsecsq in sr

   cout<<endl;
   cout<<"radians to degrees=rtd="<<rtd<<endl;
   cout<<"solid angle of healpix pixel in sr = "<<solid_angle_pixel<<endl;
   cout<<"solid angle 4 arcsecsq used in DAtool in sr = "<<solid_angle_datool<<endl;
   cout<<"healpix/DAtool solid angle= "<< solid_angle_pixel/solid_angle_datool<<endl;
   cout<<"arcmin sq="<<sa_arcminsq<<" sr"<<endl;
   cout<<"arcdeg sq="<<sa_arcdegsq<<" sr"<<endl;
   cout<<"healpix pixel_arcminsq="<<pixel_arcminsq<< " arcminsq "<<endl;
   cout<<"healpix pixel side="<<pixel_arcmin_side <<"  arcmin"   <<endl;
   cout<<endl;

   double keV_to_erg = 1.602e-9;


   double l_centre,  b_centre, radius_min, radius_max;
   int pixel_selected;

   // from intensity maps: centre 337,45   above centre b=64-74-> radius = 20-30  below centre b=23-38 


   int nebin=4;
       nebin=8;
   
   valarray<double>spectrum,emin,emax;
   
   valarray<double>spectrum1,spectrum2,spectrum3;
   
   spectrum .resize(nebin);
   spectrum1.resize(nebin);
   spectrum2.resize(nebin);
   spectrum3.resize(nebin);

   valarray<double> thermal_spectrum;
   thermal_spectrum.resize(nebin);
 
   emin     .resize(nebin);
   emax     .resize(nebin);

   valarray<double>sum;
   sum      .resize(nebin);


 
   int nused;

   
   int newplot=1;
   int iregion;
   int iebin;
   

   for(iregion=1;iregion<=3;iregion++)
   {
   
   string region="bubbles";
   //region="inside" ;
   //         region="outside";
   
   l_centre=337;
   b_centre=45;

   /*
   if(region=="bubbles"){radius_min=20;  radius_max=30;}
   if(region=="inside" ){radius_min= 0;  radius_max=20;}
   if(region=="outside"){radius_min=35;  radius_max=45;}
   */
   
   if(iregion==1){radius_min=20;  radius_max=30; region="bubbles";}
   if(iregion==2){radius_min= 0;  radius_max=20; region="inside" ;}
   if(iregion==3){radius_min=35;  radius_max=45; region="outside";}
   
   

   int all_pixels=0;
   if(all_pixels==1)
   {
    radius_min=0;    
    radius_max=360;
   }
  ///////////////////////////////////////////////////////

   int verbose=0;
  

   
 
  
   
   
   sum=0;
   nused=0;

  for (int ipix=0; ipix<inmap_1.Npix(); ipix++)
  {
   pointing_ = inmap_1.pix2ang(ipix);

   l  =     pointing_.phi  *rtd;
   b  = 90.-pointing_.theta*rtd;

   pixel_selected = pixel_select(l,b,l_centre,b_centre, radius_min,radius_max,verbose);


   if(pixel_selected==1 && inmap_1[ipix]>0)
   {
   

    nused++;
   
    sum[0] += inmap_1[ipix];   
    sum[1] += inmap_2[ipix];   
    sum[2] += inmap_3[ipix];   
    sum[3] += inmap_4[ipix];
    
    sum[4] += inmap_5[ipix];   
    sum[5] += inmap_6[ipix];   
    sum[6] += inmap_7[ipix];   
    sum[7] += inmap_8[ipix];
    
   }


   

   if(verbose==1||ipix<10||ipix>inmap_1.Npix()-10)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" l="<<l<<" b="<<b
       <<" inmap_1= "<<inmap_1[ipix]
       <<" inmap_2= "<<inmap_2[ipix]
       <<" inmap_3=" <<inmap_3[ipix]
       <<" inmap_4=" <<inmap_4[ipix]
       <<" inmap_5= "<<inmap_5[ipix]
       <<" inmap_6= "<<inmap_6[ipix]
       <<" inmap_7=" <<inmap_7[ipix]
       <<" inmap_8=" <<inmap_8[ipix]
     
       <<endl;

 
   
   
  }    // ipix
  
  cout<<"------------------------------------"<<endl;
  cout<< "region="<<region<<" l_centre="<<l_centre<<" b_centre="<<b_centre<<" radius_min="<<radius_min<<" radius_max="<<radius_max<<endl;

  cout<<"sum[0]         =" <<sum[0]         <<endl;
  cout<<"sum[1]         =" <<sum[1]         <<endl;
  cout<<"sum[2]         =" <<sum[2]         <<endl;
  cout<<"sum[3]         =" <<sum[3]         <<endl;
  
  cout<<"sum[4]         =" <<sum[4]         <<endl;
  cout<<"sum[5]         =" <<sum[5]         <<endl;
  cout<<"sum[6]         =" <<sum[6]         <<endl;
  cout<<"sum[7]         =" <<sum[7]         <<endl;
  cout<<"nused          =" <<nused          <<endl;
  
  
 

 

 
  double e1=0.2;
  double de=0.5;
  for(iebin=0;iebin<nebin;iebin++){emin[iebin]=e1+de*iebin; emax[iebin]=emin[iebin]+de;}
   
  
  for(iebin=0;iebin<nebin;iebin++){cout<< "emin="<<emin[iebin]<<" emax="<<emax[iebin]<<" summed input spectrum="      <<sum    [iebin]<<" cm-2 sr-1 s-1 ";cout<<endl;}
  
  spectrum = sum * (emin+emax)/2 *  keV_to_erg / de           * solid_angle_pixel;
  for(iebin=0;iebin<nebin;iebin++){cout<< "emin="<<emin[iebin]<<" emax="<<emax[iebin]<<" total energy spectrum="      <<spectrum[iebin]<<" erg cm-2 s-1 keV-1 ";cout<<endl;}
    
  spectrum = sum * (emin+emax)/2 *  keV_to_erg / de / nused *   sa_arcdegsq;
  for(iebin=0;iebin<nebin;iebin++){cout<< "emin="<<emin[iebin]<<" emax="<<emax[iebin]<<" average intensity spectrum=" <<spectrum[iebin]<<" erg cm-2 deg-2 s-1 keV-1   ";cout<<endl;}

  if(iregion==1) spectrum1 =spectrum;
  if(iregion==2) spectrum2 =spectrum;
  if(iregion==3) spectrum3 =spectrum;
  
  cout<<endl<<" ; ----------- idl"<<endl;
  
  cout<<"emin=[";
  cout<<emin[0];
  for(iebin=1;iebin<nebin;iebin++)cout<<", " <<emin[iebin];
  cout<<"] ;idl";
  cout<<endl;

  cout<<"emax=[";
  cout<<emax[0];
  for(iebin=1;iebin<nebin;iebin++)cout<<", " <<emax[iebin];
  cout<<"] ;idl";
  cout<<endl;

  
  cout<<"spectrum=[";
  cout<<spectrum[0];
  for(iebin=1;iebin<nebin;iebin++)cout<<", " <<spectrum[iebin];
  cout<<"] ; idl";
  cout<<endl;

  //--------------------------------------------------------------------------------

  // thermal emission
  
 double em=.03; // cm-6 pc
 double T=5e6;  // K
 double E=1;    // keV
 
 double thermal_emission_=thermal_emission(em,T,E);
 cout<<"em="<<em<<" T="<<T<<" E="<<E<<" thermal_emission_="<<thermal_emission_<<" erg cm-2       sr-1 s-1 keV-1"<<endl;

 thermal_emission_ *= sa_arcdegsq;
 
 cout<<"em="<<em<<" T="<<T<<" E="<<E<<" thermal_emission_="<<thermal_emission_<<" erg cm-2 arcdegsq-1 s-1 keV-1"<<endl;


 
 for(iebin=0;iebin<nebin;iebin++)
 {
  E=(emin[iebin]+emax[iebin])/2;
  thermal_emission_=thermal_emission(em,T,E);
  cout<<"em="<<em<<" T="<<T<<" E="<<E<<" thermal_emission_="<<thermal_emission_<<" erg cm-2       sr-1 s-1 keV-1"<<endl;

  thermal_emission_ *= sa_arcdegsq;
  thermal_spectrum[iebin]=thermal_emission_;
  
 
  cout<<"em="<<em<<" T="<<T<<" E="<<E<<" thermal_emission_="<<thermal_emission_<<" erg cm-2 arcdegsq-1 s-1 keV-1"<<endl;
 }

  cout<<"thermal_spectrum=[";
  cout<<thermal_spectrum[0];
  for(iebin=1;iebin<nebin;iebin++)cout<<", " <<thermal_spectrum[iebin];
  cout<<"] ; idl";
  cout<<endl;

  int logaxes=1; // 0=linlin  1=linlog 2=loglin  3=loglog

  // see /afs/propagate/c/gitlab/stellarics/solar_ic_driver.cc
  // xstyle=1, ystyle=1 forces exact range
  
  

  if(newplot==1)
  {

    double emin_plot  =  0.1;
    double emax_plot  =  5.;
    
   if(logaxes==0)cout<<" plot,(emin+emax)/2,spectrum ,xtitle='energy, keV', ytitle=' intensity,  erg cm!e-2!n deg!e-2!n s!e-1!n keV!e-1!n',charsize=1.6,"
		   <<"xrange=["<<emin_plot<<","<<emax_plot<<"],yrange=[1e-13,1e-9], xstyle=1,ystyle=1"
		   <<"; idl"<<endl;

   if(logaxes==1)cout<<" plot,(emin+emax)/2,spectrum ,xtitle='energy, keV', ytitle=' intensity,  erg cm!e-2!n deg!e-2!n s!e-1!n keV!e-1!n',charsize=1.6,"
		   <<"xrange=["<<emin_plot<<","<<emax_plot<<"],yrange=[1e-13,1e-9], xstyle=1,ystyle=1,/ylog,/nodata"
		   <<"; idl"<<endl;

   if(logaxes==2)cout<<" plot,(emin+emax)/2,spectrum ,xtitle='energy, keV', ytitle=' intensity,  erg cm!e-2!n deg!e-2!n s!e-1!n keV!e-1!n',charsize=1.6,"
		   <<"xrange=["<<emin_plot<<","<<emax_plot<<"],yrange=[1e-13,1e-9], xstyle=1,ystyle=1,/xlog"
		   <<"; idl"<<endl;

   if(logaxes==3)cout<<" plot,(emin+emax)/2,spectrum ,xtitle='energy, keV', ytitle=' intensity,  erg cm!e-2!n deg!e-2!n s!e-1!n keV!e-1!n',charsize=1.6,"
		   <<"xrange=["<<emin_plot<<","<<emax_plot<<"],yrange=[1e-13,1e-9], xstyle=1,ystyle=1,/xlog,/ylog"
		   <<"; idl"<<endl;

   newplot=0;

   } // if newplot
   
   //  if(loglog==0)cout<<" plot,(emin+emax)/2,spectrum ,xtitle='energy, keV', ytitle='E!e2!n X intensity, MeV!e2!n cm!e-2!n sr!e-1!n s!e-1!n MeV!e-1!n',charsize=1.3,"
   //		   <<"xrange=["<<emin[0]<<","<<emax[nebin-1]<<"],yrange=[1e-13,1e-8], xstyle=1,ystyle=1"
   //		   <<"; idl"<<endl;
  
  for(iebin=0;iebin<nebin;iebin++)
  {
   cout<<"esegment=["            <<emin[iebin] << "," <<    emax[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   
   if(iregion==1)cout<<"spectrumsegment=[" <<spectrum1[iebin] << "," <<spectrum1[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   if(iregion==2)cout<<"spectrumsegment=[" <<spectrum2[iebin] << "," <<spectrum2[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   if(iregion==3)cout<<"spectrumsegment=[" <<spectrum3[iebin] << "," <<spectrum3[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   
   cout<<"oplot,esegment,spectrumsegment,linestyle="<<iregion-1;
   cout<<" ; idl";  cout<<endl;
  }

 

 
   
  
 

  } // iregion


    // plot subtracted spectra   bubbles - outside
  cout<<" subtract region1-region3"<<endl;
  
   for(iebin=0;iebin<nebin;iebin++)
  {
   cout<<"esegment=["            <<emin[iebin] << "," <<    emax[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   
   cout<<"spectrumsegment=[" <<spectrum1[iebin]-spectrum3[iebin] << "," <<spectrum1[iebin]-spectrum3[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   
   
   cout<<"oplot,esegment,spectrumsegment,linestyle=4,thick=3";
   cout<<" ; idl";  cout<<endl;
  }

 // thermal spectrum

  
  cout<<"oplot,(emin+emax)/2,thermal_spectrum,linestyle=3         "<<"; idl"<<endl;
   for(iebin=0;iebin<nebin;iebin++)
  {
   cout<<"esegment=["                    <<emin[iebin] << "," <<            emax[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   cout<<"spectrumsegment=[" <<thermal_spectrum[iebin] << "," <<thermal_spectrum[iebin]<<"]";  cout<<" ; idl";  cout<<endl;
   cout<<"oplot,esegment,spectrumsegment,linestyle=3";  cout<<" ; idl";  cout<<endl;
  }
   

   cout<< "WRITE_JPEG, 'spectrum.jpg', TVRD(), QUALITY=25"; cout<<" ; idl";  cout<<endl;

   //cout<<"end ; idl";  cout<<endl;   // not wanted for running idl plot.pro
     
 //   outfile="!healpix_maps_test.fits";
  

 //   cout<<"writing inmap_1  to "<<outfile<<endl;
    //    write_Healpix_map_to_fits(outfile,inmap_1,datatype);

  
    cout<<endl<<" ==== healpix_skymap complete"<<endl;

  return 0;                 


  }
}
////////////////////////////////////////////////////////
int main()
{
  cout<<"healpix_spectra.cc"<<endl;
  healpix_skymap();
  cout<<"end healpix_spectra.cc"<<endl;
  return 0;
}
