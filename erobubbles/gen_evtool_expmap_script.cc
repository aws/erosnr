

using namespace std;
#include<iostream>
#include<cstdio>   
#include<cstring>          
#include<valarray>
#include"fitsio.h"

#include "healpix_map.h"
#include "healpix_map_fitsio.h"
#include "arr.h"
#include "fitshandle.h"
#include "pointing.h"  
#include "trafos.h"

#include <sstream>
#include <string>

#include "Skyfields.h"

///////////////////////////////////////////////////////
int gen_evtool_expmap_sciserver_script(  valarray<string> event_filename_list)
{
  //  cout<<"gen_evtool_expmap_sciserver_script"<<endl;
  /*
evtool image=yes events=no eventfiles=data1/eventfiles/eb02_121048_020_EventList_c946.fits outfile=evtool_eb02_121048.fits size=auto center_position=auto rebin=800
expmap inputdatasets=data1/eventfiles/eb02_121048_020_EventList_c946.fits templateimage=evtool_eb02_121048.fits emin=0.5 emax=2.0 mergedmaps=expmap_eb02_121048_eb1.fits
  */

  

  int ienergy1=0;
  int ienergy2=2;

  ienergy1=3;
  ienergy2=6;

  for (int ienergy=ienergy1;ienergy<=ienergy2;ienergy++)
  {


  for(int ilist=0;ilist<event_filename_list.size();ilist++)
  {

  string event_filename;
  
  event_filename=event_filename_list[ilist];
  
  

  string  tile_erass_id,tile_emb,tile_ra, tile_dec,tile_c94x;

  tile_erass_id=event_filename.substr( 2,2);       // start, length
  tile_emb     =event_filename.substr( 0,2);
  tile_ra      =event_filename.substr( 5,3);
  tile_dec     =event_filename.substr( 8,3);
  tile_c94x    =event_filename.substr(27,3);

  cout<<"event_filename="<<event_filename;

  cout<<" tile_emb=" <<tile_emb;
  cout<<" tile_erass_id=" <<tile_erass_id;
  cout<<" tile_ra=" <<tile_ra;
  cout<<" tile_dec="<<tile_dec;
  cout<<" tile_c94x="<<tile_c94x<<endl;

  // run in ~/workspace

  string event_file_directory = "~/workspace/eroProductEventlist/"; //AWS20220117 for running from Storage
  string    skymaps_directory;
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.2_10/";
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.613_.693/";
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.3_0.8/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.8_1.5/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_1.5_8.0/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter

            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.3_0.8/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.8_1.5/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_1.5_8.0/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter

            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.3_0.8/TM12346/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_0.8_1.5/TM12346/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter
            skymaps_directory = "Storage/aws/persistent/skymaps/with_eventlists_E_1.5_8.0/TM12346/";      //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter

           if(tile_c94x=="946")
	   {
            if(ienergy==0)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/release20211215/E_0.3_0.8/TM12346/";   //<<<<<<<<<<<<<<<<<<<  parameter //AWS20220117 for running from Storage
            if(ienergy==1)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/release20211215/E_0.8_1.5/TM12346/";   //<<<<<<<<<<<<<<<<<<<  parameter //AWS20220117 for running from Storage
            if(ienergy==2)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/release20211215/E_1.5_8.0/TM12346/";   //<<<<<<<<<<<<<<<<<<   parameter //AWS20220117 for running from Storage

            if(ienergy==3)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/release20211215/E_0.3_0.6/TM12346/";   //<<<<<<<<<<<<<<<<<<<  parameter //AWS20220117 for running from Storage
            if(ienergy==4)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/release20211215/E_0.6_1.0/TM12346/";   //<<<<<<<<<<<<<<<<<<<  parameter //AWS20220117 for running from Storage
            if(ienergy==5)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/release20211215/E_1.0_2.5/TM12346/";   //<<<<<<<<<<<<<<<<<<   parameter //AWS20220117 for running from Storage
            if(ienergy==6)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/release20211215/E_2.5_8.0/TM12346/";   //<<<<<<<<<<<<<<<<<<   parameter //AWS20220117 for running from Storage
	   }
          

           if(tile_c94x=="947")
	   {
	    if(ienergy==0)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/c947/release20211215/E_0.3_0.8/TM12346/";   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter  c947
	    if(ienergy==1)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/c947/release20211215/E_0.8_1.5/TM12346/";   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter  c947
	    if(ienergy==2)skymaps_directory = "~/workspace/Storage/aws/persistent/skymaps/c947/release20211215/E_1.5_8.0/TM12346/";   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   parameter  c947
	   }

  string event_full_filename;
  event_full_filename= event_file_directory +tile_dec +"/" + tile_ra + "/"  + "EXP_" + tile_c94x + "/" + tile_emb  +tile_erass_id + "_"+tile_ra +tile_dec +"_020_EventList_" +"c" + tile_c94x + ".fits";
  cout<<"event_full_filename="<<event_full_filename<<endl;

  string evtool_outfile=skymaps_directory+"evtool_"+event_filename;
  string evtool_command_line;

  evtool_command_line="evtool eventfiles="+event_full_filename;
  evtool_command_line+=" image=yes events=yes size=auto center_position=auto rebin=800";


  //  evtool_command_line+=" emin=0.5 emax=2.0";
  
  string eminemax=" emin=0.2 emax=10.0";
         eminemax=" emin=0.613 emax=0.693";
         eminemax=" emin=0.3   emax=0.8"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=0.8   emax=1.5"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=1.5   emax=8.0"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter  

         eminemax=" emin=0.3   emax=0.8"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=0.8   emax=1.5"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=1.5   emax=8.0"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter 

         eminemax=" emin=0.3   emax=0.8"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=0.8   emax=1.5"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=1.5   emax=8.0"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter 

         eminemax=" emin=0.3   emax=0.8"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=0.8   emax=1.5"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
         eminemax=" emin=1.5   emax=8.0"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter

        eminemax=" emin=0.3   emax=0.8"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter

	if(ienergy==0) eminemax=" emin=0.3   emax=0.8"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
	if(ienergy==1) eminemax=" emin=0.8   emax=1.5"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
	if(ienergy==2) eminemax=" emin=1.5   emax=8.0"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter

	if(ienergy==3) eminemax=" emin=0.3   emax=0.6"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
	if(ienergy==4) eminemax=" emin=0.6   emax=1.0"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
	if(ienergy==5) eminemax=" emin=1.0   emax=2.5"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter
	if(ienergy==6) eminemax=" emin=2.5   emax=8.0"  ; //<<<<<<<<<<<<<<<<<<<<<<<<<< parameter


  evtool_command_line+=eminemax;

  string telid="'1 2 3 4 5 6 7'";            //<<<<<<<<<<<<<<<<<<<<<<<<<<< parameter   all TMs
         telid="'1 2 3 4 6'"    ;            //<<<<<<<<<<<<<<<<<<<<<<<<<<< parameter : no light leak TMs

  evtool_command_line+=" telid="+telid;

  evtool_command_line+=" repair_gtis=yes";  // esass release 20211215

  evtool_command_line+=" outfile="+evtool_outfile;

  cout<<evtool_command_line<<endl;

  string expmap_mergedmaps=skymaps_directory+"expmap_"+event_filename;
  string expmap_command_line="expmap ";

  expmap_command_line+=" inputdatasets="+event_full_filename;

  expmap_command_line+=" templateimage="+evtool_outfile;
  
  expmap_command_line+=eminemax;

  expmap_command_line+=" withdetmaps=yes";      // esass release 20211215

  expmap_command_line+=" mergedmaps="+expmap_mergedmaps;

  cout<<expmap_command_line<<endl;
 
  //  cout<<"after ilist="<<ilist<<endl;

  }// for ilist

  }// for ienergy

  //  cout<<"end gen_evtool_expmap_sciserver_script"<<endl;

  return 0;
};


///////////////////////////////////////////////////////
int gen_evtool_expmap_script(  valarray<string> event_filename_list)
{
  cout<<"gen_evtool_expmap_script"<<endl;
  /*
evtool image=yes events=no eventfiles=data1/eventfiles/eb02_121048_020_EventList_c946.fits outfile=evtool_eb02_121048.fits size=auto center_position=auto rebin=800
expmap inputdatasets=data1/eventfiles/eb02_121048_020_EventList_c946.fits templateimage=evtool_eb02_121048.fits emin=0.5 emax=2.0 mergedmaps=expmap_eb02_121048_eb1.fits
  */

  for(int ilist=0;ilist<event_filename_list.size();ilist++)
  {

  string event_filename;
  
  event_filename=event_filename_list[ilist];
  
  string event_file_directory = "data1/eventfiles/";
  string    skymaps_directory = "data1/skymaps/with_eventlists/";
  skymaps_directory = "data1/skymaps/with_eventlists_E_0.2_10/";

  string evtool_outfile=skymaps_directory+"evtool_"+event_filename;
  string evtool_command_line;
  evtool_command_line="evtool image=yes events=yes size=auto center_position=auto rebin=800";
  //  evtool_command_line+=" emin=0.5 emax=2.0";
  evtool_command_line+=" emin=0.2 emax=10.0";
  evtool_command_line+=" eventfiles="+event_file_directory+event_filename;
  evtool_command_line+=" outfile="+evtool_outfile;

  cout<<evtool_command_line<<endl;

  string expmap_mergedmaps=skymaps_directory+"expmap_"+event_filename;
  string expmap_command_line="expmap ";
  //  expmap_command_line+=" emin=0.5 emax=2.0";
  expmap_command_line+=" emin=0.2 emax=10.0";
  expmap_command_line+=" inputdatasets="+event_file_directory+event_filename;
  expmap_command_line+=" templateimage="+evtool_outfile;
  expmap_command_line+=" mergedmaps="+expmap_mergedmaps;

  cout<<expmap_command_line<<endl;
 
  //  cout<<"after ilist="<<ilist<<endl;

  }// for ilist

  cout<<"end gen_evtool_expmap_script"<<endl;

  return 0;
};

/////////////////////////////////////////////////////////
int read_event_filename_list(string event_filename_list_infile, valarray<string> &event_filename_list,int nline,int debug)

{
//based partly on ~/propagate/c/gitlab/galplot/source/Galdef.cc
  cout<<"read_event_file_list"<<endl;
  cout<<"event_filename_list_infile="<<event_filename_list_infile<<endl;
  cout<<"nline="<<nline<<endl;
  cout<<"debug="<<debug<<endl; 
 
  char  filename[200];
  strcpy(filename,event_filename_list_infile.c_str());
  
  FILE *ft;
  ft=fopen(filename,"r");
  if(ft==NULL) {
      cout<<"no input event filename list called "<<filename<<endl; return -1;
  }
 


//int Galdef::read_galdef_parameter(char *filename, char *parstring, char *value)


// char input[37];
   char input[126];
   int stat=0;

// ls -1 e*fits >> input_testfile
// this give 36 chars due to \0 or newline  but want 35 hence need to truncate

//  /afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/eventlists/eventlist/051/153/EXP_946/eb02_153051_020_EventList_c946.fits  wc:   125 chars but 124 works below
// this gives 124 chars:

   string teststringlength="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/eventlists/eventlist/051/153/EXP_946/eb02_153051_020_EventList_c946.fits";
   cout<<"testing filename string length:"<<teststringlength.size()<<endl;

   // Stroustrup p. 595
   string::size_type ifits;
   ifits=teststringlength.find("fits");
   cout<< "testing position of string fits: ifits="<<ifits<<endl;
   cout<< "hence string length = ifits+4 = "<<ifits+4<<endl;

valarray<string> instring;

instring.resize(nline);

for(int iline=0;iline<nline;iline++)
{

    //https://www.cplusplus.com/reference/cstdio/fgets/
    
 fgets(input,800,ft);
    

 // cout<<"printf:"<<endl;
 // printf("%s",input);       // string is \0 terminated
 
 if(debug==1)cout<<"strlen(input)="<<strlen(input)<<endl;

instring[iline]=input;

 if(debug==1)cout<<"instring= "<<instring[iline]<<endl;

cout<<"iline="<<iline<<" instring="<<instring[iline]<<endl;

 if(debug==1)cout<<"instring length before resize="<<instring[iline].length()<<endl;

 // instring[iline].resize(35);

   
   ifits=instring[iline].find("fits");
   cout<< "testing position of string fits in instring: ifits="<<ifits<<endl;
   
   int instring_length = ifits+4;
   cout<< "hence instring length = ifits+4 = "<<instring_length<<endl;
   
 

 instring[iline].resize(instring_length); 

 if(debug==1)cout<<"instring length after resize="<<instring[iline].length()<<endl;

/*
 cout<<"testing with explicit event file"<<endl;
strcpy(input,"eb01_108051_020_EventList_c946.fits");
cout<<"strcmp "<<strcmp(input,"eb01_108051_020_EventList_c946.fits")<<endl;
*/

  } // iline


 if(debug==1)
 {
 cout<<"---------testing open on instring"<<endl;

for(int iline=0;iline<nline;iline++)
  {
 
    if(debug==1)
    {
     cout<<"iline="<<iline<<" "<<instring[iline]<<endl;
     cout<<"length="<<instring[iline].length()<<endl;
     cout<<"instring="<<instring[iline]<<endl;
     cout<< "c_str=" <<instring[iline].c_str()  <<endl;
     }

 FILE *eventft;
       eventft=fopen(instring[iline].c_str(),"r");

  if(eventft!=NULL) cout<<"opened file "   <<instring[iline]<< " c_str=" <<instring[iline].c_str()<<" eventft="<<eventft  <<endl;

  if(eventft==NULL) cout<<"no file called "<<instring[iline]<< " c_str=" <<instring[iline].c_str()  <<endl;
  
  fclose(eventft);

}
 }// debug==1
  
   fclose(ft); 

   event_filename_list=instring;

   cout<<"number of event files read="<<event_filename_list.size()<<endl;

   return stat;
}

////////////////////////////////////////////////////////

void read_event_files(string event_filename,valarray<double>&ra,valarray<double>&dec,valarray<double>&energy,int &SKYFIELD,int debug)
{
  cout<<"read_event_files"<<endl;
  cout<<"event_filename= "<<event_filename<<endl;

  int verbose=0;

  fitsfile *fptr=0;
  int status=0; // need to initialize to 0  or error
  int hdunum;
  int hdutype;
  long nrows;
  int ncols;
  int colnum;
  int casesen=1;
  char   colname[100];
  string colnamestring;
  int datatype=TDOUBLE;
  long firstrow,firstelem,nelements;
  double *nulval=NULL;
  int    *anynul=NULL;

#define NMAXROWS 10000000
       double RA[NMAXROWS],DEC[NMAXROWS],ENERGY[NMAXROWS];
//     double RA[10000000],DEC[10000000],ENERGY[10000000];



  //int fits_open_file( fitsfile **fptr, char *filename, int mode, int *status)
  fits_open_file( &fptr, event_filename.c_str(), READONLY, &status);
   

  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);

  hdunum=2;
  fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);
  cout<<"hdutype="<<hdutype<<endl;
  cout<<"BINARY_TBL="<<BINARY_TBL<<endl;

  /*  
int fits_get_num_rows(fitsfile *fptr, long *nrows, int *status)
int fits_get_num_cols(fitsfile *fptr, int  *ncols, int *status)

Get the number of rows or columns in the current FITS table. The number of rows is given by the NAXIS2 keyword and the number of columns is given by the TFIELDS keyword in the header of the table.

_______________________________________________________________
int fits_get_colnum(fitsfile *fptr, int casesen, char *template,
                    int *colnum, int *status)
int fits_get_colname(fitsfile *fptr, int casesen, char *template,
                    char *colname, int *colnum, int *status)
  */

  fits_get_num_rows(fptr, &nrows, &status);
  fits_get_num_cols(fptr, &ncols, &status);
  cout<<"nrows="<<nrows<<" ncols="<<ncols<<endl;
  if(nrows>NMAXROWS){cout<<"too many rows for array size "<<NMAXROWS<< " return!"<<endl; ra.resize(0); return ;}

  colnamestring="RA";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  //                             colnamestring.c_str() did not work as argument (but did for fits_open_read above
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;


  //int fits_read_col(fitsfile *fptr, int datatype, int colnum, long firstrow,
  //     long firstelem, long nelements, void *nulval, void *array, 
  //     int *anynul, int *status)

  firstrow =1;
  firstelem=1;
  nelements=nrows;
  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RA, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1) for(int i=0;i<nelements;i++)   cout<<RA[i]<<endl;


  colnamestring="DEC";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, DEC, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1)for(int i=0;i<nelements;i++)   cout<<DEC[i]<<endl;

  colnamestring="PI";  //energy in keV
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;


  fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, ENERGY, 
		 anynul, &status);

  cout<<"fits_read_col status="<<status<<endl;

if(debug==1)
  for(int i=0;i<nelements;i++)   cout<<"i="<<i<<" RA="<<RA[i]<<" DEC="<<DEC[i]<<" ENERGY="<<ENERGY[i]<<endl;

  cout<<"copy to valarray"<<endl;



  ra    .resize(nelements);               
  dec   .resize(nelements);
  energy.resize(nelements);

  cout<<"ra.size()="<<ra.size()<<endl;

  for(int i=0;i<nelements;i++)
  {
    ra    [i]=RA    [i];
    dec   [i]=DEC   [i];
    energy[i]=ENERGY[i]*1.0e-3; // eV->keV 
  }


    char comment[400];
    
    fits_read_key(fptr,TINT,"SKYFIELD" ,&SKYFIELD ,  comment,&status) ; 
    cout<<"FITS read SKYFIELD status= "<<status<<" SKYFIELD="<<SKYFIELD<<endl;


  fits_close_file( fptr,  &status);
   

  cout<<"fits_close_file status="<<status<<endl;
  fits_report_error(stdout, status);
  

  cout<<"end read_event_files"<<endl;
  return;
}

////////////////////////////////////////////////////////

int healpix_skymap( valarray<string> event_filename_list,int order, int coordsys,string outfile, double Emin, double Emax, int debug)
{

  
  cout<<endl<< "=== healpix_skymap"<<endl<<endl;

  Skyfields skyfields;

  int smapnr_;
  double ra_min_, ra_max_, ra_cen_;
  double dec_min_,dec_max_,dec_cen_;
  int    status;

  skyfields.read_SKYMAPS();

  // test 
  smapnr_=1180;
  skyfields.get_skyfield( smapnr_,
                    ra_min_, ra_max_, ra_cen_,
		    dec_min_, dec_max_, dec_cen_,
			  status);

   cout<<" test from skyfields.get_skyfield:"
       <<" ra_min="  <<ra_min_<<" ra_max=" << ra_max_ << " ra_cen=" <<ra_cen_
       <<" dec_min="<<dec_min_<<" dec_max="<<dec_max_ <<" dec_cen="<<dec_cen_
       <<" smapnr="<<smapnr_ <<" status="<<status<<endl;

   
    
  PDT datatype = PLANCK_FLOAT64; // HealPix defines it this way
      datatype = PLANCK_FLOAT32;
  
 
  arr<double>data; // for HealPix array class
  
  int hdu;
  
  int ncolnum,colnum;
 
 
  valarray<double> ra,dec,energy;
  int SKYFIELD;
  int ipix;
  double total_events_selected;

  int intile,outtile;
  int usetile;
      usetile=1;// 1=only in tile 2=only out of tile 3=both
  int useevent;
  //==================== 

  ncolnum=1;

  cout<<"output healpix events skymap= "<<outfile<<endl;
  cout<<"healpix order="<<order<<endl;
  cout<<"healpix coordsys="<<coordsys;
  if(coordsys==1) cout<< " Equatorial";
  if(coordsys==2) cout<< " Galactic"<<endl;
  if(coordsys >2) {cout<<" invalid coordsys"<<endl; return 1;}
  cout<<"Emin="<<Emin<<" Emax="<<Emax<<" keV"<<endl;
  cout<<"debug="<<debug<<endl;

  fitshandle out;  
  out.create("!"+outfile); // ! to overwrite

  arr<string> colname;
  colname.alloc(ncolnum);

  for(int j=0;j<ncolnum;j++)   colname[j]="events";

  Healpix_Map<double> map_RING_out(order,RING);
  prepare_Healpix_fitsmap(out,map_RING_out, datatype, colname);
  
  out.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
     
  colnum=1;
 
  cout<< "map_RING_out: ";
  cout<<" Npix  = "<<map_RING_out.Npix(); 
  cout<<" Nside = "<<map_RING_out.Nside();
  cout<<" Order = "<<map_RING_out.Order();
  cout<<" Scheme= "<<map_RING_out.Scheme()<<endl; // 0 = RING, 1 = NESTED

  data.alloc(map_RING_out.Npix());
  
  pointing pointing_;
  pointing pointing_galactic;
  
  double l,b;
  double rtd=180./acos(-1.); // radians to degrees 180/pi
  double dtr=acos(-1.)/180.; // degrees to radians pi/180

  double iepoch=2000.;
  double oepoch=2000.;
  Trafo trafo(iepoch,oepoch,Equatorial,Galactic); // cxxsupport  trafos.cc trafos.h
  rotmatrix rm = trafo.Matrix();



  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data[ipix]=0;

  for(int ieventfile=0;ieventfile<event_filename_list.size();ieventfile++)
  {
    cout<<"ieventfile="<<ieventfile<<endl;

    read_event_files(event_filename_list[ieventfile],ra,dec,energy,SKYFIELD,debug);

  smapnr_=SKYFIELD;
  skyfields.get_skyfield( smapnr_,
                    ra_min_, ra_max_, ra_cen_,
		    dec_min_, dec_max_, dec_cen_,
			  status);

    cout<<" from skyfields.get_skyfield:"
       <<" ra_min="  <<ra_min_<<" ra_max=" << ra_max_ << " ra_cen=" <<ra_cen_
       <<" dec_min="<<dec_min_<<" dec_max="<<dec_max_ <<" dec_cen="<<dec_cen_
       <<" smapnr="<<smapnr_ <<endl;

    

  cout<<"ra.size()="<<ra.size()<<endl;
  if(ra.size()==0) {cout<<"ra.size=0 signals too many events in file, return 1"<<endl; return 1;}

  intile =0;
  outtile=0;

  for(int i=0;i<ra.size();i++) 
  {  
 
   if(energy[i] >= Emin && energy[i] <= Emax)

   {

     if(debug==1)cout<<" testing event i="<<i
         <<" ra_min="  <<ra_min_<<" ra="<<ra[i]<<" ra_max=" << ra_max_ << " ra_cen=" <<ra_cen_
	 <<" dec_min="<<dec_min_<<" dec="<<dec[i]<<" dec_max="<<dec_max_ <<" dec_cen="<<dec_cen_
         <<" smapnr="<<smapnr_ ;

     useevent=0;

     if(     ra[i] >=  ra_min_ &&  ra[i] <= ra_max_
         && dec[i] >= dec_min_ && dec[i] <=dec_max_)
	
       {  
	 if(debug==1) cout<<" testing event in  tile"    <<endl;
	if(usetile==1|| usetile==3) useevent=1;
        intile++;
       }

       else 

       {
	 if(debug==1)cout<<" testing event not in  tile"<<endl;
	if(usetile==2||usetile==3) useevent=1;
        outtile++;
       }

     if(debug==1)     cout<<"useevent= "<<useevent<<endl;

  if(useevent==1)
  {

   pointing_.phi   =       ra[i] *dtr;
   pointing_.theta = (90.-dec[i])*dtr; 

   // trafos.h
   /*! Transforms the pointing \a ptg and returns the result. */
   //    pointing operator() (const pointing &ptg) const;

   pointing_galactic=trafo(pointing_);
   l =     pointing_galactic.phi   * rtd;
   b = 90.-pointing_galactic.theta * rtd;

   if(coordsys==1)
   ipix=map_RING_out.ang2pix(pointing_);

  if(coordsys==2)
   ipix=map_RING_out.ang2pix(pointing_galactic);

   data[ipix]+=1;

   if(debug==1)
   cout<<"i="<<i<<" ra="<<ra[i]<<" dec="<<dec[i]<<" energy="<<energy[i]
       <<" pointing_galactic.phi="<<pointing_galactic.phi<<" pointing_galactic.theta="<<pointing_galactic.theta
       <<" l="<<l<<" b="<<b
       <<" ipix="<<ipix<<" data[ipix]="<<data[ipix]<<endl;
 

     }// useevent==1
   } //if energy
  } // event

  cout<<"number of events selected in tile="<<intile<< " out of tile="<<outtile<< " total="<<intile+outtile<<endl;

  } //eventfile



  total_events_selected=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) total_events_selected+=data[ipix];
  cout<<"total events selected in data="<<total_events_selected<<endl; 

  cout<<"writing healpix skymap to "<< outfile << endl;
  out.write_column(colnum,data); // fitshandle.h

  out.close();                                                         

  map_RING_out.Set(data,RING); // data is zero size after call!

  total_events_selected=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) total_events_selected+=map_RING_out[ipix];
  cout<<"total events selected in  map="<<total_events_selected<<endl; 

  /*
  for (int ipix=0; ipix<map_RING_out.Npix(); ipix++)
  {
   pointing_ = map_RING_out.pix2ang(ipix);

   l =     pointing_.phi  *rtd;
   b = 90.-pointing_.theta*rtd;

   if(debug==1)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" l="<<l<<" b="<<b<<" map_RING_out[ipix]= "<<map_RING_out[ipix]<<endl;
  }
  */

 ////////////////////////////////////////////////////////

  cout<<endl<<" ==== healpix_skymap complete"<<endl;

 return 0;
  
};


////////////////////////////////////////////////////////
int main()
{ 
  string event_filename;
  valarray<double>ra,dec,energy;
  int SKYFIELD;

  valarray<string> event_filename_list;
  string healpix_event_filename;
  int coordsys;
  int order;
  double Emin,Emax;
  int debug;
  
  int test_read_event_files=0;

  int nevent_files=4;

  event_filename_list.resize(nevent_files);

  event_filename_list[0]="eb02_096054_020_EventList_c946.fits";
  event_filename_list[1]="eb02_096054_020_EventList_c946_copy1.fits";  
  event_filename_list[2]="eb02_096054_020_EventList_c946_copy2.fits";
  
  int i=0;
  //                     erass ra dec               proc
  //                 owner both   if >90 dec=90-xxx
  //                       |   |  |
  event_filename_list[i]="eb01_108051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_096054_020_EventList_c946.fits"; i++;
    
  
  nevent_files=62;
  event_filename_list.resize(nevent_files);
     
  i=0;

  //                     erass ra dec               proc
  event_filename_list[i]="eb01_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_001180_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_001180_020_EventList_c946.fits"; i++;

  event_filename_list[i]="eb01_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_125048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_125048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_129048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_129048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_133048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_133048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_136048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_136048_020_EventList_c946.fits"; i++;

  event_filename_list[i]="eb01_140048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_140048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_144048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_144048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_148048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_148048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_152048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_152048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_156048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_156048_020_EventList_c946.fits"; i++;


  event_filename_list[i]="eb01_108051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_108051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_112051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_112051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_116051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_116051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_119051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_119051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_123051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_123051_020_EventList_c946.fits"; i++;


  event_filename_list[i]="em01_127051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_127051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_131051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_131051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_135051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_135051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_138051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_138051_020_EventList_c946.fits"; i++;

  
  event_filename_list[i]="em01_142051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_142051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_146051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_146051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_150051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_150051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_153051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_153051_020_EventList_c946.fits"; i++;

  
  event_filename_list[i]="eb01_157051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_157051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_161051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_161051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_165051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_165051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_169051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_169051_020_EventList_c946.fits"; i++;
  /*
  event_filename_list[i]="eb01_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_001180_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_001180_020_EventList_c946.fits"; i++;
  */

 if(test_read_event_files==1)
 for(int ieventfile=0;ieventfile<nevent_files;ieventfile++)
 {
   debug=1;
   read_event_files(event_filename_list[ieventfile],ra,dec,energy,SKYFIELD,debug);

  cout<<"ra.size()="<<ra.size()<<endl;
  for(int i=0;i<ra.size();i++) 
   cout<<"i="<<i<<" ra="<<ra[i]<<" dec="<<dec[i]<<" energy="<<energy[i]<<endl;
 }

 /////////////
  string event_filename_list_infile;
  

  cout<<"reading event filename list from "<<event_filename_list_infile<<endl;
 
  int nline;
  nline=63;
  nline=189;
  int testfile=5;
  testfile=12;
  testfile=13;

  testfile=9; // used for erass1_2 on sciserver

  testfile=1; // erass1
  testfile=2; // erass2
  testfile=3; // erass3
  testfile=4; // erass4

  testfile=1; //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< parameter  erass1 or 2 or 3 or 4



  int c9xx=946;

  for(testfile=1;testfile<=4;testfile++)
  {

  //  testfile=14;

  if(testfile==5){event_filename_list_infile="input_testfile5"; nline=4878;} // all  ieventfile= 726 seg 

  if(testfile==6){event_filename_list_infile="input_testfile6"; nline=2261;} //em01  370 seg
  if(testfile==7){event_filename_list_infile="input_testfile7"; nline=2261;} //em02 370 seg
  if(testfile==8){event_filename_list_infile="input_testfile8"; nline=2261;} // em01 -1r: reversed iventfile=  no seg, completed but status 104 for some files 

  if(testfile==9){event_filename_list_infile="input_testfile9"; nline=4878;} // all -1r  flat<<<<<<<<<<<<<< used for sciserver erass 1_2

  if(testfile==10){event_filename_list_infile="input_testfile10"; nline=1;} // one file which gave status 104 with testfile9 but here ok
  if(testfile==11){event_filename_list_infile="input_testfile11"; nline=2261;} // em02 -1r: reversed 1241 status=104 1020 status 0  withfitsclose: no status 104 but seg at 1310

  if(testfile==12){event_filename_list_infile="eventfile_list_erass1_2"; nline=4878;}

  if(testfile==13){event_filename_list_infile="eventfile_list_erass1_2_flat"; nline=5;}


  if(testfile==14){event_filename_list_infile="eventfile_list_erass3_prelim"; nline=2396;} //erass3 with 2 weeks to go: script prelim

  if(testfile== 1){event_filename_list_infile="eventfile_list_erass1"; nline=2439;} //erass1
  if(testfile== 2){event_filename_list_infile="eventfile_list_erass2"; nline=2439;} //erass2
  if(testfile== 3){event_filename_list_infile="eventfile_list_erass3"; nline=2439;} //erass3  list made in sciserver ls -1R eroProductEventlist|grep em03>event_list_erass3 grep eb03...
  if(testfile== 4){event_filename_list_infile="eventfile_list_erass4"; nline=2439;} //erass4  list made in sciserver ls -1R eroProductEventlist|grep em04>event_list_erass3 grep eb04...

  if(c9xx==947)
  {
   if(testfile== 1){event_filename_list_infile="eventfile_list_erass1_c947"; nline=2439;} //erass1 c947
   if(testfile== 2){event_filename_list_infile="eventfile_list_erass2_c947"; nline=2439;} //erass2 c947   not yet there
   if(testfile== 3){event_filename_list_infile="eventfile_list_erass3_c947"; nline=2439;} //erass3 c947   not yet there
   if(testfile== 4){event_filename_list_infile="eventfile_list_erass4_c947"; nline=2439;} //erass4 c947   not yet there
  }
  debug=0;


  
  read_event_filename_list(event_filename_list_infile,event_filename_list,nline,debug);

  cout<<"event_filename_list.size()= "<<event_filename_list.size()<<endl;
 /////////////
 

 order=12;
 coordsys=1;   // 1=equatorial, 2=Galactic
 healpix_event_filename="healpix_events_equatorial.fits";
 Emin=0.3;
 Emax=2.3;
 debug=0;



 // healpix_skymap(event_filename_list,order, coordsys, healpix_event_filename, Emin,Emax, debug);

 if(0)
 gen_evtool_expmap_script          (event_filename_list);    // for he3srv

 gen_evtool_expmap_sciserver_script(event_filename_list);    // for SciServer

  }// for testfile

  return 0;
}
