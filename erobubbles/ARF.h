class ARF
{
 public:

  valarray<double> energ_lo, energ_hi, specresp;
 

  void  read_ARF(int verbose);
  void print_ARF(int verbose);
  
  double get_ARF(double energy,int verbose, int &status);
  double get_ARF(double energy_min,double energy_max,double spectral_index,int verbose, int &status);

};
