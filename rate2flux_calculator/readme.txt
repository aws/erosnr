##############################################
##############################################
This script is for converting eROSITA observed count rates into flux using xspec to simulate spectrum. 
The ECF and the Particle background will be estimated as side products. 
Pls note that the ECF here \npresents the ratio between the instr-bkg free rates and observed flux (absorbed flux). Before applying ECF, the instr-bkg need to be deducted from the rates forehand.


Usage: Download the script 'rate2flux_cal.py' with the directory 'supplement'.
       run 'python rate2flux_cal.py' with interactive prompts. Recommanded for the first attempt.
       For example:
       ###
       >The total observed image rates in the region (cts/s/TM):60
       >The region area (deg^2):49
       >The PATTERN number (1,3,7,15):15
       >The energy ranges
       >emin (keV):1.0
       >emax (keV):2.3
       >The spectral model (apec or po):po
       >The index of the powerlaw:2.0
       >If data is absorbed. The number density of galactic absorption (10^22 cm^-2):0.02
       ####
       Alternatively, one-line command is possible. 
       For example: >>python3 rate2flux_cal.py 60.0 49 15 1.0 2.3 po 2.0 0.02
###############################################
#zhengxy@mpe.mpg.de 
###############################################
Updated on 09.05.2023
