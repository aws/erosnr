import numpy as np
from astropy.io import fits
from xspec import *
import sys
import os
import glob


if len(sys.argv)>5:
    rates = float(sys.argv[1])
    Area = float(sys.argv[2])
    PAT = int(sys.argv[3])
    emin = float(sys.argv[4])
    emax = float(sys.argv[5])
    mod = sys.argv[6]
    par = float(sys.argv[7])
    if mod=='apec':
        par2 = float(sys.argv[8])
        Nh = float(sys.argv[9])
    else:
        Nh = float(sys.argv[8])##cm^-2
else:
    print("#####################rate2flux_cal.py############################")
    print('Usage:')
    print("This script is for converting eROSITA observed count rates into flux. \nThe ECF and the Particle background will be estimated as side products. \nPlease input the configuration following the prompt. Pls note that the ECF here \npresents the ratio between the instr-bkg free rates and observed flux (absorbed flux). \nBefore applying ECF, the instr-bkg need to be deducted from the rates forehand.\nOne-line command is possible. For example: >>python3 rate2flux_cal.py 60.0 49 15 1.0 2.3 po 2.0 0.02")
    print("################################################################")
    print("")
    rates = float(input("> The total observed image rates in the region (cts/s/TM):"))
    Area = float(input('> The region area (deg^2):'))
    PAT = int(input('> The PATTERN number (1,3,7,15):'))
    print('The energy ranges')
    emin = float(input('> emin (keV):'))
    emax = float(input('> emax (keV):'))
    mod = input('> The spectral model (apec or po):')
    if mod.lower() =='apec':
        par = float(input('>>>> The temperature of apec (keV):'))
        par2 = float(input('>>>> The abundance of apec:'))
    elif mod.lower() =='po':
        par = float(input('>>>> The index of the powerlaw:'))
    Nh = float(input('> If data is absorbed. The number density of galactic absorption (10^22 cm^-2):'))

ero_hdu = fits.open("./supplement/ARF_nocorr.fits")
ero_area = ero_hdu[1].data['SPECRESP']
ene_ero = (ero_hdu[1].data['ENERG_LO']+ero_hdu[1].data['ENERG_HI'])/2.

ero_a = np.interp((emin+emax)/2., ene_ero, ero_area)


###in pyXspec
AllModels.clear()
if mod=='apec':
   # m = Model('tbabs*(apec+po)', setPars={1:1.23854E-05, 2:56.1125, 3:5.72093E-17, 4:0, 5:0.899934, 6:4.15673, 7:4.84667E-02})
    m = Model('tbabs*'+mod, setPars={1:Nh, 2:par, 3:par2, 4:0, 5:10})
elif mod=='po':
    m = Model('tbabs*'+mod, setPars={1:Nh, 2:par, 3:0.2067})
    #m = Model(mod, setPars={1:par, 2:0.243})

RMF = "./supplement/020_RMF_00001_PAT%i_c020.fits" % PAT
ARF = './supplement/ARF_nocorr.fits'
if not os.path.exists('./temp'):
    os.system('mkdir ./temp')
if os.path.exists('./temp/delete.fak'):
    os.remove('temp/delete.fak')
fs1 = FakeitSettings(response=RMF, arf=ARF, fileName="temp/delete.fak" , exposure=8000) 
AllModels.calcFlux("%.3f %.3f" %(emin, emax))
ero_flux = AllModels(1).flux[0]
AllData.fakeit(1,fs1)
AllData.clear()
sp=Spectrum("temp/delete.fak")
sp.response=RMF
sp.response.arf=ARF
sp.ignore("**-%.3f %.3f-**" %(emin, emax))
sp.notice("%.3f" % emin)
ero_rate = sp.rate[0]/7
AllData.clear()


#######
expolist=[   0.3,        0.5,       0.8,     1.65, 3.65, 6.5]
exprange=[[0.2, 0.4], [0.4, 0.6], [0.6, 1.0], [1.0,2.3], [2.3, 5.0], [5.0, 8.0]]


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

whichexp = find_nearest(expolist, np.mean([emin,emax]))
expmin=exprange[whichexp][0]
expmax=exprange[whichexp][1]

sp=Spectrum("supplement/TM8_FWC_c020_ClosedSpec_pat15_noRMF.fits", respFile='supplement/020_RMF_00001_PAT15_c020.fits')

#sp=Spectrum("./supplement/TM4_FWC_c020_ClosedSpec_pat15.fits", respFile=RMF)
sp.ignore("**-%.3f %.3f-**" %(emin, emax))
sp.notice("%.3f" % emin)
fwc=sp.rate[0]
AllModels.clear()
AllData.clear()
fwc=fwc/0.85958/5

e = np.array([0.225, 0.275, 0.325, 0.375, 0.425, 0.475, 0.525, 0.575, 0.625, 0.675, 0.725, 0.775, 0.825, 0.875, 0.925, 0.975, 1.025, 1.075, 1.125, 1.175, 1.225, 1.275, 1.325, 1.375, 1.425, 1.475, 1.525, 1.575, 1.625, 1.675, 1.725, 1.775, 1.825, 1.875, 1.925, 1.975, 2.025, 2.075, 2.125, 2.15, 2.45, 2.75, 3.05, 3.35, 3.65, 3.95, 4.25, 4.55, 4.85, 5.15, 5.45, 5.75, 6.05, 6.35, 6.65, 6.95, 7.25, 7.55, 7.85, 8.15])
ratio_o = np.array([1.8520705, 1.8513403, 1.8448596, 1.837516, 1.8300223, 1.8227761, 1.8160498, 1.8104875, 1.8049129, 1.799467, 1.794008, 1.7885846, 1.7831936, 1.7778018, 1.7730541, 1.7738411, 1.7759697, 1.7780826, 1.7802227, 1.7828081, 1.7885109, 1.7947931, 1.8010942, 1.8074903, 1.8138849, 1.8203733, 1.8301166, 1.8408506, 1.8517871, 1.8627787, 1.8739761, 1.8852715, 1.8966691, 1.9082743, 1.9199555, 1.9318457, 1.943852, 1.9560089, 1.9706204, 1.9741597, 2.104183, 2.2572906, 2.4072585, 2.4767308, 2.5429156, 2.6231232, 2.711133, 2.809875, 2.9350338, 3.0733962, 3.2314858, 3.4237328, 3.6403668, 3.8757517, 3.9961681, 4.0778136, 4.1611567, 4.2470016, 4.336235, 4.394268])*0.98
ratio_list = np.interp(np.linspace(emin, emax, 20), e, ratio_o)
ratio = np.mean(ratio_list)
#print(fwc)
#print(ratio, ratio2, r1, r2)
print('')
print('')
print('###################################################################################################')
print('##########################################INPUT####################################################')
print('Energy range:  %.3f-%.3f keV'% (emin, emax))
print('Input rates:   %f cts/s/TM in %.2f degree^2' %(rates, Area))
print('PATTERN:       %i' % (PAT))
if mod.lower() == 'apec':
    print('Model:         APEC of temperature %.3f and abundance %.3f' % (par, par2))
if mod.lower() == 'po':
    print('Model:         Powerlaw of index %.3f' % (par))
print('Absorption:    %.3e cm^-2' % (Nh*1e22))
print('')
print('##########################################OUTPUT##################################################')
print('The ECF (specfic for this model and energy):                   %e (cts/s/TM)/(erg/cm^2/s)' % (ero_rate/ero_flux))
print('In case the input rates has subtracted the instr background:')
print('     The flux:                                                 %e erg/s/cm^2' % (rates/ero_rate*ero_flux))
print('     The Surface brightness:                                   %e erg/s/cm^2/deg^2' % (rates/ero_rate*ero_flux/Area))
print('#################################################################################################')
print('In case the input rates has NOT subtracted instr background:')
print('     The net flux:                                             %e erg/s/cm^2 (%e erg/s/cm^2/keV)' % ((rates-fwc*ratio*Area)/ero_rate*ero_flux, (rates-fwc*ratio*Area)/ero_rate*ero_flux/(emax-emin)))
print('     The net surface brightness:                               %e erg/s/cm^2/deg^2' % ((rates-fwc*ratio*Area)/ero_rate*ero_flux/Area))
print('     The estimated particle background rates:                  %.3e cts/s/TM/deg^2' % (fwc*ratio))
print('     The ratio of unvignetted exposure and vignetted exposure: %f' % ratio)
if Area:
    print('Comments: in the consisdered sky area (%.2f deg^2), the instr background takes (roughly) %.3e cts/s/TM in the total %.3e cts/s/TM (~%.2f %%)' % (Area, fwc*ratio*Area, rates, fwc*ratio*Area/rates*100) )

print('##################################################################################################')
print('##################################################################################################')


